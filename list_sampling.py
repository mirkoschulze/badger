# Badger - A Schema-Based Data file_iter
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from math import ceil, floor, sqrt
from random import randint, randrange, paretovariate, shuffle

from buffered_reader import BufferedReader
from helpers import (
    resolve_string,
    shift_lower_bound_to_conform_to_precision
)
from number_sampling import (
    random_pyramid_layer,
    draw_paretovariate
)


def open_file_reader(path, sep):
    return BufferedReader(path, sep)


def determine_num_of_records_in_file(path, sep):
    with open_file_reader(path, sep) as file:
        size = sum([1 for _ in file])
    return size


def extract_sample(path, sep, chosen_indizes):
    indizes = chosen_indizes.copy()
    indizes.sort()
    sample = []
    item_counter = 0
    with open_file_reader(path, sep) as file:
        for item in file:
            while (
                len(indizes) != 0 and
                indizes[0] == item_counter
            ):
                sample.append(item)
                indizes.pop(0)
            if len(indizes) == 0:
                break
            item_counter += 1
    assert len(indizes) == 0
    shuffle(sample)
    return sample


def sample_from_file_uniform(path, sep, num):
    size = determine_num_of_records_in_file(path, sep)
    chosen = [randrange(size) for _ in range(num)]
    return extract_sample(path, sep, chosen)


def sample_from_file_desc(path, sep, num):
    size = determine_num_of_records_in_file(path, sep)
    chosen = [size - random_pyramid_layer(size) for _ in range(num)]
    return extract_sample(path, sep, chosen)


def sample_from_file_asc(path, sep, num):
    size = determine_num_of_records_in_file(path, sep)
    chosen = [random_pyramid_layer(size) - 1 for _ in range(num)]
    return extract_sample(path, sep, chosen)


def read_cyclic_from_file(path, sep, num):
    if num < 1:
        raise Exception("Number of items to read must be at least one.")
    output = []
    with open_file_reader(path, sep) as file:
        for item in file:
            output.append(item)
            if len(output) == num:
                return output
    size = len(output)
    full_repeats = (num - size) // size
    tail_size = (num - size) % size
    for _ in range(full_repeats):
        output += output
    return output + output[:tail_size]


def sample_from_file_paretovariate_with_resampling(
    path, sep, num, alpha=1.0, factor=1.0
):
    candidates = []
    num_of_records_in_file = determine_num_of_records_in_file(path, sep)
    chosen_indizes = draw_indizes_paretovariate_with_resampling(
        num_of_records_in_file - 1, num, alpha, factor
    )
    return extract_sample(path, sep, chosen_indizes)


def draw_indizes_paretovariate_with_resampling(
    maximum, num, alpha, factor
):
    if maximum < 0 :
        raise ValueError("Maximum should be a positive integer.")
    lower_bound = shift_lower_bound_to_conform_to_precision(0, 0)
    chosen_indizes = []
    for i in range(num):
        j = 0
        timeout = 1000
        while j < timeout:
            draw = draw_paretovariate(lower_bound, alpha, factor)
            draw_rounded = int(round(draw, 0))
            if draw <= maximum:
                chosen_indizes.append(draw_rounded)
                break
    if len(chosen_indizes) != num:
        raise Exception("Paretovariate list sampling does not work.")
    return chosen_indizes


if __name__ == '__main__':
    pass