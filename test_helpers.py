# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import unittest
from helpers import *


class HelpersTest(unittest.TestCase):
    """
    Only procedures that are used in production are tested.
    """

    def test_resolve_string_on_hello_world_has_no_effect(self):
        self.assertTrue(resolve_string("Hello world!") == "Hello world!")


    def test_resolve_string_on_enquoted_string_removes_quotes(self):
        self.assertTrue(resolve_string("\"Hello world!\"") == "Hello world!")


    def test_resolve_string_on_quotes_only_returns_empty_string(self):
        self.assertTrue(resolve_string("\"\"") == "")


    def test_resolve_string_on_escaped_string_returns_resolved_string(self):
        self.assertTrue(resolve_string("\\\\\\{") == "\\{")


    def test_resolve_string_on_enquoted_and_escaped_string_returns_resolved_string(self):
        self.assertTrue(resolve_string("\"\\\\\\{\"") == "\\{")


    def test_resolve_string_raises_exception_on_wrong_escaping(self):
        with self.assertRaises(Exception):
            resolve_string("\\")


    def test_enquote_and_escape_on_hello_world_has_no_effect(self):
        self.assertTrue(enquote_and_escape("Hello world!") == "\"Hello world!\"")


    def test_enquote_and_escape_on_quotes_is_handeled_correctly(self):
        self.assertTrue(enquote_and_escape("\"\"") == "\"\\\"\\\"\"")


    def test_resolve_docstring_while_ignoring_linebreaks(self):
        s = """
            Foo  
            bar.  
        """
        result = resolve_docstring(s, ignore_linebreaks=True)
        self.assertTrue(result == "Foo bar.")


if __name__ == '__main__':
    unittest.main()