# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import unittest
from main import generate


class ListImportTest(unittest.TestCase):

    def test_list_raw(self):
        sample = generate(
            "# i = list import testres/test.list\n{i}",
            1
        )
        item = sample[0]
        file = open("testres/test.list", "r")
        contents = file.readlines()
        file.close()
        self.assertTrue(item in [line.strip() for line in contents])
       

    def test_list_with_inline_seperator(self):
        sample = generate(
            "# i = list import testres/test_with_inline_sep.list sep=\"\\\\\"\n{i}",
            1
        )
        item = sample[0]
        self.assertTrue(item in ["foo", "bar", "baz"])

    @unittest.skip("Not implemented yet.")
    def test_list_with_multiline_seperator(self):
        sample = generate(
            "# i = list import testres/test_with_multiline_sep.list sep=\"\\n***\\n\"\n{i}",
            1
        )
        item = sample[0]
        self.assertTrue(item == "foo\nbar" or item == "another\nfoobar")


if __name__ == '__main__':
    unittest.main()