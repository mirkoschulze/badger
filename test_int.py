# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import unittest
from hist import *
from helpers import is_int, resolve_docstring
from input_exception import InputException
import matplotlib.pyplot as plt
from main import generate


class IntTest(unittest.TestCase):

    def test_int_raw(self):
        sample = generate(
            "# i = int\n{i}",
            100_000
        )
        self.assertTrue(is_int(sample))
        hist = NaturalCenteredHist(sample, -30, 30, 0)
        self.assertTrue(hist.freq_at_is_between(0, 3500, 4500))
        self.assertTrue(hist.freqs_at_are_between([-20, +20], 300, 700))
        self.assertTrue(hist.diffs_between_are_between(-20, -10, 0, 500))
        self.assertTrue(hist.diffs_between_are_between(+10, +20, -500, 0))
        self.assertTrue(hist.freqs_between_are_between(-10, 0, 2000, 4500))
        self.assertTrue(hist.freqs_between_are_between(0, 10, 2000, 4500))
        self.assertTrue(hist.freqs_between_are_between(-30, -20, 25, 750))
        self.assertTrue(hist.freqs_between_are_between(20, 30, 25, 750))
        self.assertTrue(hist.left_out_is_between(50,150))
        self.assertTrue(hist.right_out_is_between(50,150))


    def test_int_with_min_equals_max(self):
        sample = generate(
            "# i = int min=42 max=42\n{i}",
            1
        )
        n = float(sample[0])
        self.assertTrue(n == 42)

        sample = generate(
            "# i = int min=42 max=42 dist=desc\n{i}",
            1
        )
        n = float(sample[0])
        self.assertTrue(n == 42)

        sample = generate(
            "# i = int min=42 max=42 dist=asc\n{i}",
            1
        )
        n = float(sample[0])
        self.assertTrue(n == 42)


    def test_int_with_max_smaller_than_min(self):
        with self.assertRaises(InputException):
            generate(
                "# i = int min=0 max=-42\n{i}",
                1
            )
        with self.assertRaises(InputException):
            generate(
                "# i = int min=0 max=-42 dist=desc\n{i}",
                1
            )
        with self.assertRaises(InputException):
            generate(
                "# i = int min=0 max=-42 dist=asc\n{i}",
                1
            )
        with self.assertRaises(InputException):
            generate(
                "# i = int min=0 max=-42 dist=normal\n{i}",
                1
            )
        with self.assertRaises(InputException):
            generate(
                "# i = int min=0 max=-42 dist=pareto\n{i}",
                1
            )


    def test_int_with_alternative_notation(self):
        sample = generate(
            "# i = int min=0 max=10 dist=DESC\n{i}",
            1000
        )
        self.assertTrue(is_int(sample))
        hist = NaturalCenteredHist(sample, 0, 10)
        self.assertTrue(hist.freq_at_is_between(0, 100, 250))
        self.assertTrue(hist.diffs_between_are_between(0, 10, -300, 30))


    def test_int_with_invalid_min_raises_exception(self):
        code = resolve_docstring("""
            # MyInt = int min=foo
            {MyInt}
        """)
        with self.assertRaises(InputException):
            generate(code, 1)


###


    def test_int_with_neg_max_only(self):
        with self.assertRaises(InputException):
            generate(
                "# i = int max=-200\n{i}",
                1
            )


    def test_int_with_neg_max_only(self):
        with self.assertRaises(InputException):
            generate(
                "# i = int max=-200 dist=desc\n{i}",
                1
            )


    def test_int_with_normal_dist_and_min_close_to_mu(self):
        with self.assertRaises(InputException):
            generate(
                "# i = int dist=normal min=0 mu=0\n{i}",
                1
            )


###


    def test_int_uniform(self):
        sample = generate(
            "# i = int min=0 max=99\n{i}",
            100000
        )
        self.assertTrue(is_int(sample))
        hist = NaturalCenteredHist(sample, 0, 99)
        self.assertTrue(hist.freqs_between_are_between(0, 99, 800, 1200))
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.right_out() == 0)


    def test_int_uniform_with_specified_mu(self):
        sample = generate(
            "# i = int min=0 max=99 mu=42\n{i}",
            100_000
        )
        self.assertTrue(is_int(sample))
        hist = NaturalCenteredHist(sample, 0, 99)
        self.assertTrue(hist.freqs_between_are_between(0, 99, 800, 1200))
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.right_out() == 0)


###


    def test_int_with_desc(self):
        sample = generate(
            "# i = int min=0 max=10 dist=desc\n{i}",
            100_000
        )
        self.assertTrue(is_int(sample))
        hist = NaturalCenteredHist(sample, 0, 10)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(0, 16000, 17000))        
        self.assertTrue(hist.diffs_between_are_between(0, 10, -2000, 200))
        self.assertTrue(hist.freq_at_is_between(10, 1000, 2000))
        self.assertTrue(hist.right_out() == 0)


    def test_int_with_asc(self):
        sample = generate(
            "# i = int min=0 max=10 dist=asc\n{i}",
            100_000
        )
        self.assertTrue(is_int(sample))
        hist = NaturalCenteredHist(sample, 0, 10)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(10, 16000, 17000))        
        self.assertTrue(hist.diffs_between_are_between(0, 10, -200, 2000))
        self.assertTrue(hist.freq_at_is_between(0, 1000, 2000))
        self.assertTrue(hist.right_out() == 0)


###


    def test_int_with_normal_dist_and_custom_mu(self):
        sample = generate(
            "# i = int dist=normal mu=-42.5\n{i}",
            100_000
        )
        self.assertTrue(is_int(sample))
        hist = NaturalCenteredHist(sample, -72.5, -12.5, 0)
        self.assertTrue(hist.freq_at_is_between(-42.5, 3500, 4500))
        self.assertTrue(hist.left_out_is_between(0,500))
        self.assertTrue(hist.right_out_is_between(0,500))
        self.assertTrue(hist.freqs_between_are_between(-50, -42.5, 2500, 4500))
        self.assertTrue(hist.freqs_between_are_between(-42.5, -35, 2500, 4500))
        self.assertTrue(hist.diffs_between_are_between(-65, -50, 0, 500))
        self.assertTrue(hist.diffs_between_are_between(-35, -20, -500, 0))
        self.assertTrue(hist.freqs_between_are_between(-20, -12.5, 0, 750))
        self.assertTrue(hist.freqs_between_are_between(-72.5, -65, 0, 750))


    def test_int_with_normal_dist_and_custom_sigma(self):
        sample = generate(
            "# i = int dist=normal sigma=4.2\n{i}",
            100_000
        )
        self.assertTrue(is_int(sample))
        hist = NaturalCenteredHist(sample, -15, +15, 0)
        self.assertTrue(hist.left_out_is_between(0,100))
        self.assertTrue(hist.right_out_is_between(0,100))
        self.assertTrue(hist.freqs_between_are_between(-2, +2, 8000, 10_000))
        self.assertTrue(hist.diffs_between_are_between(-10, -2, 0, 2000))
        self.assertTrue(hist.diffs_between_are_between(+2, +10, -2000, 0))
        self.assertTrue(hist.freqs_between_are_between(-15, -10, 0, 750))
        self.assertTrue(hist.freqs_between_are_between(+10, -15, 0, 750))


    def test_int_with_normal_dist_and_bound_specs_and_custom_mu(self):
        sample = generate(
            "# i = int dist=normal mu=-42.5 min=-60\n{i}",
            100_000
        )
        self.assertTrue(is_int(sample))
        hist = NaturalCenteredHist(sample, -60, -10, 0)
        self.assertTrue(hist.left_out_is_between(0, 0))
        self.assertTrue(hist.freq_at_is_between(-60, 4000, 5000))
        self.assertTrue(hist.diffs_between_are_between(-59, -49, -100, 500))
        self.assertTrue(hist.freqs_between_are_between(-49, -37, 3000, 5000))
        self.assertTrue(hist.diffs_between_are_between(-37, -25, -500, 100))
        self.assertTrue(hist.freqs_between_are_between(-25, -10, 0, 1000))
        self.assertTrue(hist.right_out_is_between(0, 100))


    def test_int_with_normal_dist_and_bound_specs_without_mu(self):
        sample = generate(
            "# i = int dist=normal min=-22 max=42\n{i}",
            100_000
        )
        self.assertTrue(is_int(sample))
        hist = NaturalCenteredHist(sample, -20, 40)
        self.assertTrue(hist.freq_at_is_between(10, 3500, 4500))
        self.assertTrue(hist.freqs_at_are_between([-10, +30], 300, 700))
        self.assertTrue(hist.diffs_between_are_between(-10, 0, 0, 500))
        self.assertTrue(hist.diffs_between_are_between(+20, +30, -500, 0))
        self.assertTrue(hist.freqs_between_are_between(0, 10, 2000, 4500))
        self.assertTrue(hist.freqs_between_are_between(10, 20, 2000, 4500))
        self.assertTrue(hist.freqs_between_are_between(-20, -10, 25, 750))
        self.assertTrue(hist.freqs_between_are_between(30, 40, 25, 750))
        self.assertTrue(hist.left_out_is_between(50, 150))
        self.assertTrue(hist.right_out_is_between(50, 150))


###


    def test_int_with_pareto_dist(self):
        sample = generate(
            "# i = int dist=pareto\n{i}",
            100_000
        )
        self.assertTrue(is_int(sample))
        hist = NaturalCenteredHist(sample, 0, 10, 0)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(0, 45_000, 55_000))        
        self.assertTrue(hist.freqs_between_are_between(1, 5, 2000, 20_000))
        self.assertTrue(hist.freqs_between_are_between(5, 10, 500, 3000))
        self.assertTrue(hist.freq_at_is_between(10, 10, 1000))


    def test_int_with_pareto_dist_and_custom_factor_and_shifted(self):
        sample = generate(
            "# i = int dist=pareto min=-100 paretofactor=10\n{i}",
            100_000
        )
        self.assertTrue(is_int(sample))
        hist = NaturalCenteredHist(sample, -100, 0)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(-100, 8000, 10_000))        
        self.assertTrue(hist.freqs_between_are_between(-99, -80, 1000, 8000))
        self.assertTrue(hist.freqs_between_are_between(-80, -50, 100, 2000))
        self.assertTrue(hist.freqs_between_are_between(-50, -100, 50, 500))


    def test_int_with_pareto_dist_and_custom_shape_parameter(self):
        sample = generate(
            "# i = int dist=pareto paretoshape=2\n{i}",
            100_000
        )
        self.assertTrue(is_int(sample))
        hist = NaturalCenteredHist(sample, 0, 10)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(0, 70_000, 90_000))
        self.assertTrue(hist.freq_at_is_between(1, 10_000, 20_000))
        self.assertTrue(hist.freq_at_is_between(2, 2500, 7500))
        self.assertTrue(hist.freqs_between_are_between(3, 10, 100, 5000))


    def test_int_with_pareto_dist(self):
        sample = generate(
            "# i = int dist=pareto min=0 max=10\n{i}",
            100_000
        )
        self.assertTrue(is_int(sample))
        hist = NaturalCenteredHist(sample, 0, 10)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(0, 45_000, 55_000))
        self.assertTrue(hist.freq_at_is_between(1, 14_000, 20_000))
        self.assertTrue(hist.freq_at_is_between(2, 7000, 10_000))
        self.assertTrue(hist.freqs_between_are_between(3, 9, 500, 7500))
        self.assertTrue(hist.freq_at_is_between(10, 8000, 12_000))


if __name__ == '__main__':
    unittest.main()
