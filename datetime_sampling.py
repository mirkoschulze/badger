# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime, timedelta
from math import floor
from random import randrange
from re import fullmatch
from input_exception import InputException


FORMAT = {
    "yyyy-mm-dd": "%Y-%m-%d",
    "dd.mm.yyyy": "%d.%m.%Y",
    "d.m.yyyy": "%-d.%-m.%Y",
    "dd.mm.yy": "%d.%m.%y",
    "d.m.yy": "%-d.%-m.%y",
    "d. M yyyy": "%-d. %B %Y",
    "dd/mm/yyyy": "%d/%m/%Y",
    "d/m/yyyy": "%-d/%-m/%Y",
    "dd/mm/yy": "%d/%m/%y",
    "d/m/yy": "%-d/%-m/%y",
    "d M yyyy": "%-d %B %Y",
    "mm/dd/yyyy": "%m/%d/%Y",
    "m/d/yyyy": "%-m/%-d/%Y",
    "mm/dd/yy": "%m/%d/%y",
    "m/d/yy": "%-m/%-d/%y",
    "M d, yyyy": "%B %-d, %Y",
}


def random_datetime(minimum, maximum):
    delta = maximum - minimum
    int_delta = floor(delta.total_seconds())
    if int_delta < 0:
        raise InputException(
            "Date range is not properly specified."
        )
    random_second = randrange(int_delta)
    return minimum + timedelta(seconds=random_second)


def identify_date_format(date_string):
    if fullmatch(
        '\\d{4,4}-\\d?\\d-\\d?\\d',
        date_string
    ):
        return "%Y-%m-%d"
    if fullmatch(
        '\\d?\\d\\.\\d?\\d\\.\\d{4,4}',
        date_string
    ):
        return "%d.%m.%Y"
    if fullmatch(
        '\\d?\\d\\.\\d?\\d\\.\\d\\d',
        date_string
    ):
        return "%d.%m.%y"
    raise Exception("Could not identify date format.")