# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import unittest
from random import uniform
from hist import NaturalCenteredHist, TrivialHist


@unittest.skip("Not a production part of Badger.")
class HistTest(unittest.TestCase):
    """
    Some criminal insufficient testing of my histogram implementation.
    """

    def test_natural_centered_hist_implementation(self):
        hist = NaturalCenteredHist([3,-1, 42, 3.3, 3.5, 10, 101], 0, 10)
        self.assertTrue(hist.freq_at(0) == 0)
        self.assertTrue(hist.freq_at(3) == 2)
        self.assertTrue(hist.freq_at(4.49) == 1)
        self.assertTrue(hist.freq_at(10) == 1)
        self.assertTrue(hist.left_out() == 1)
        self.assertTrue(hist.right_out() == 2)


    def test_trivial_hist_implementation(self):
        hist = TrivialHist([3, 0, 42, 10, 3.42, 3.49, 42, 9.99], 0, 10, 10)
        self.assertTrue(hist.freq_at(0) == 1)
        self.assertTrue(hist.freq_at(3) == 3)
        self.assertTrue(hist.freq_at(4) == 0)
        self.assertTrue(hist.freq_at(9.1) == 2)
        self.assertTrue(hist.freq_at(10) == 2)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.right_out() == 2)


    def test_error_on_wrong_freq_access(self):
        hist = NaturalCenteredHist([], 0, 10)
        with self.assertRaises(Exception):
            hist.freq_at(-1)
      

    def test_hist_diffs(self):
        hist = NaturalCenteredHist([3,-1, 42, 3.3, 3.5, 10, 101], 0, 10)
        expected_diffs = [0, 2, -1, -1, 0, 0]
        self.assertTrue(hist.diffs_between(1,7) == expected_diffs)


    def test_freq_at_is_between(self):
        hist = NaturalCenteredHist([3,-1, 42, 3.3, 3.5, 10, 101], 0, 10)
        self.assertTrue(hist.freq_at_is_between(3, 2, 2))


    def test_freqs_at_are_between(self):
        hist = NaturalCenteredHist([3,-1, 42, 3.3, 3.5, 10, 101], 0, 10)
        self.assertTrue(hist.freqs_at_are_between([3, 4], 1, 2))


    def test_freqs_between_are_between(self):
        hist = NaturalCenteredHist([3,-1, 42, 3.3, 3.5, 10, 101], 0, 10)
        self.assertTrue(hist.freqs_between_are_between(3, 4, 1, 2))


    def test_left_out_is_between(self):
        hist = NaturalCenteredHist([3,-1, 42, 3.3, 3.5, 10, 101], 0, 10)
        self.assertTrue(hist.left_out_is_between(1, 1))


    def test_right_out__isbetween(self):
        hist = NaturalCenteredHist([3,-1, 42, 3.3, 3.5, 10, 101], 0, 10)
        self.assertTrue(hist.right_out_is_between(2, 2))


    def test_diffs_between_are_between(self):
        hist = NaturalCenteredHist([3,-1, 42, 3.3, 3.5, 10, 101], 0, 10)
        self.assertTrue(hist.diffs_between_are_between(3, 5, -1, -1))


    def test_diffs_of_are_between(self):
        hist = NaturalCenteredHist([3,-1, 42, 3.3, 3.5, 10, 101], 0, 10)
        self.assertTrue(hist.diffs_of_are_between([3, 5], -2, -2))


if __name__ == '__main__':
    unittest.main()
