# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from abc import ABC, abstractmethod
from math import floor
import matplotlib.pyplot as plt


class Hist(ABC):
    """
    An abstract histogram implementation for number lists.
    The main intention behind this implementation is to provide
    convinient methods to verify some properties regarding the histogram.

    All operations are value-related and do not base on any artifical
    list indizes. For example, freq_at(x) provides the frequency
    of the bucket containing x.

    As there is room for maneuver in the construction of the buckets,
    several concrete implementations are offered.
    """

    sample = None
    freqs = []
    left_out_ = 0
    right_out_ = 0
    left_bound = None
    right_bound = None


    @abstractmethod
    def __init__(self, sample, left_bound, right_bound, density=0):
        pass


    def __len__(self):
        return len(self.freqs)


    @abstractmethod
    def index(self, value):
        pass


    def freq_at(self, value):
        return self.freqs[self.index(value)]


    def left_out(self):
        return self.left_out_


    def right_out(self):
        return self.right_out_


    def freqs_between(self, start_value, stop_value):
        if stop_value - start_value < 0:
            return []
        startindex = self.index(start_value)
        stopindex = self.index(stop_value)
        return self.freqs[startindex:stopindex + 1]


    def diffs_of(self, l):
        return [l[i+1] - l[i] for i in range(len(l) - 1)]


    def diffs_between(self, start_value, stop_value):
        return self.diffs_of(self.freqs_between(start_value, stop_value))


    def freq_at_is_between(self, value, lower_bound, upper_bound):
        freq = self.freq_at(value)
        return lower_bound <= freq and freq <= upper_bound


    def freqs_at_are_between(self, value_list, lower_bound, upper_bound):
        freqs = [self.freq_at(value) for value in value_list]
        return all(map(
            lambda n: lower_bound <= n and n <= upper_bound,
            freqs
        ))


    def freqs_between_are_between(
        self, start_value, stop_value, lower_bound, upper_bound
    ):
        return all(map(
            lambda n: lower_bound <= n and n <= upper_bound,
            self.freqs_between(start_value, stop_value)
        ))


    def left_out_is_between(self, lower_bound, upper_bound):
        return lower_bound <= self.left_out() and self.left_out() <= upper_bound


    def right_out_is_between(self, lower_bound, upper_bound):
        return lower_bound <= self.right_out() and self.right_out() <= upper_bound


    def diffs_between_are_between(self, start_value, stop_value, lower_bound, upper_bound):
        return all(map(
            lambda n: lower_bound <= n and n <= upper_bound,
            self.diffs_between(start_value, stop_value)
        ))


    def diffs_of_are_between(self, value_list, lower_bound, upper_bound):
        freqs = [self.freq_at(value) for value in value_list]
        return all(map(
            lambda n: lower_bound <= n and n <= upper_bound,
            self.diffs_of(freqs)
        ))


    def plot(self, clip=False):
        buckets = self.buckets()
        sample = self.sample
        if clip:
            sample = [
                x for x in self.sample 
                if self.left_bound <= x and x <= self.right_bound
            ]
            sample += [
                self.left_bound for x in self.sample
                if x < self.left_bound
            ]
            sample += [
                self.right_bound for x in self.sample
                if x > self.right_bound
            ]
        plt.hist(sample, buckets)
        plt.show()


    @abstractmethod
    def buckets(self):
        pass



class TrivialHist(Hist):
    """
    An implementation of the abstract Hist class.
    
    The buckets are constructed in the obvious way based on the specified
    bounds and number of buckts. Both bounds are treated inclusive.
    """

    bucket_width = None

    def __init__(self, sample, left_bound, right_bound, num_of_buckets=100):
        if num_of_buckets < 1:
            raise ValueError(
                "There must be at least one bucket."
            )
        self.sample = [float(x) for x in sample]
        self.left_bound = left_bound
        self.right_bound = right_bound
        self.bucket_width = (right_bound - left_bound) / num_of_buckets
        
        self.freqs = [0] * num_of_buckets
        for value in self.sample:
            if value < self.left_bound:
                self.left_out_ += 1
                continue
            if value > self.right_bound:
                self.right_out_ += 1
                continue
            index = self.index(value)
            self.freqs[index] += 1


    def index(self, value):
        if value < self.left_bound or value > self.right_bound:
            print(self.right_bound)
            raise ValueError("Argument out of range.")
        if value == self.right_bound:
            return len(self.freqs) - 1
        return int((value - self.left_bound) / self.bucket_width)


    def buckets(self):
        buckets = [
            self.left_bound + i * self.bucket_width
            for i in range(len(self.freqs))
        ]
        buckets.append(self.right_bound)
        return buckets



class NaturalCenteredHist(Hist):
    """
    An implementation of the abstract Hist class.

    The buckets are centered around the integers or rounded decimals.
    Width and number of buckets are chosen based on the bound
    and precision specification.
    """

    precision = None

    def __init__(self, sample, left_bound, right_bound, precision=0):
        if precision < 0 or precision > 3:
            raise ValueError(
                "Only precisions between 0 and 3 supported."
            )
        self.sample = [float(x) for x in sample]
        self.precision = precision

        left_bound_normalized = roundHalfUp(left_bound, precision)
        right_bound_normalized = roundHalfUp(right_bound, precision)
        self.left_bound = round(
            left_bound_normalized - (0.5 / 10 ** precision),
            precision + 1
        )
        self.right_bound = round(
            right_bound_normalized + (0.5 / 10 ** precision),
            precision + 1
        )
        
        ran = right_bound_normalized - left_bound_normalized
        num_of_buckets = round(ran * 10 ** precision) + 1
        self.freqs = [0] * num_of_buckets
        for value in self.sample:
            if value < self.left_bound:
                self.left_out_ += 1
                continue
            if value >= self.right_bound:
                self.right_out_ += 1
                continue
            index = self.index(value)
            self.freqs[index] += 1


    def index(self, value):
        index = (value - self.left_bound) * 10 ** self.precision
        if index < 0 or index >= len(self.freqs):
            raise ValueError("Argument out of range.")
        return int(index)


    def buckets(self):
        buckets = [
            self.left_bound + i / 10 ** self.precision
            for i in range(len(self.freqs) + 1)
        ]
        return buckets




def roundHalfUp(x, p):
    """
    Rounds a value with strategy 'up on half' (NOT commercial rounding).
    """
    x_shifted = x * (10 ** p)
    return floor(x_shifted + 0.5) / (10 ** p)


if __name__ == "__main__":
    pass
    #hist = Hist([1,0.42, 0.13,0.01,0.4, 0.9, 0.99,0.35], 0.1, 1.0001, 1)
    #print(hist.freq_at(1))
    #hist.plot()
