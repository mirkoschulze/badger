# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import unittest
from helpers import is_int, resolve_docstring
from input_exception import InputException
from main import generate


class ArrayTest(unittest.TestCase):

    def test_array_raw(self):
        code = resolve_docstring("""
            # l = list 3 {int + ","}
            #
            {l}
        """)
        result = generate(code, 1)[0]
        splitted_res = result.split(",")
        self.assertTrue(
            len(splitted_res) == 4 and
            is_int(splitted_res[:-1]) and
            splitted_res[3] == ""
        )


    def test_array_embedding(self):
        code = resolve_docstring("""
            # l = list 3 int
            #
            {"," l}
        """)
        result = generate(code, 1)[0]
        splitted_res = result.split(",")
        self.assertTrue(
            len(splitted_res) == 3 and
            is_int(splitted_res)
        )


    def test_array_embedding_with_mixed_identifiers(self):
        code = resolve_docstring("""
            # l = list 3 int
            # foo = "foo"
            #
            {"," foo l}
        """)
        result = generate(code, 1)[0]
        splitted_res = result.split(",")
        self.assertTrue(
            len(splitted_res) == 4 and
            splitted_res[0] == "foo" and
            is_int(splitted_res[1:])
        )


    def test_array_embeddings_smart_delim_feature(self):
        code = resolve_docstring("""
            # MyInt = int | ""
            # L = list 20 MyInt
            # gap = ""
            # foo = "foo"
            #
            {"," L gap foo gap}
        """)
        result = generate(code, 1)[0]
        splitted_res = result.split(",")
        self.assertTrue(
            len(splitted_res) < 20 and
            splitted_res[-1] == "foo" and
            is_int(splitted_res[:-1])
        )


    def test_array_individuals(self):
        code = resolve_docstring("""
            # l << list 3 int
            #
            {"," l}:{"," l}
        """)
        result = generate(code, 1)[0]
        result_splitted = result.split(":")
        self.assertTrue(result_splitted[0] == result_splitted[1])


    def test_sub_array(self):
        code = resolve_docstring("""
            # A << list 10 int
            # B << A[3-5]
            #
            {"," A}:{"," B}
        """)
        result = generate(code, 1)[0]
        result_splitted = result.split(":")
        result_left = result_splitted[0].split(",")
        result_right = result_splitted[1].split(",")
        self.assertTrue(result_left[3:6] == result_right)


    def test_sub_array_cycle_raises_error(self):
        code = resolve_docstring("""
            # A << B[3-5]
            # B << A[3-5]
            #
            Some
        """)
        with self.assertRaises(InputException):
            generate(code, 1)


if __name__ == '__main__':
    unittest.main()