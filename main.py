# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import sys
from analyze import parse, analyze
from input_exception import raise_input_exception, InputException
from interpret import Interpreter

from lark import tree


def parse_and_print(code):
    ast = parse(code)
    make_png(ast, "tree.png")
    print(ast)


def make_png(ast, filename):
    tree.pydot__tree_to_png(ast, filename)


def analyze_and_print(code):
    print(analyze(code)[1])


def generate(code, num_of_rows=1):
    output = []
    body, var_specs, individuals = analyze(code)
    interpreter = Interpreter(
        body,
        individuals,
        num_of_rows,
        var_specs
    )
    for i in range(num_of_rows):
        output.append(interpreter.interpret())
    return output


def generate_and_print(code, num_of_rows, newline=False):
    body, var_specs, individuals = analyze(code)
    interpreter = Interpreter(
        body,
        individuals,
        num_of_rows,
        var_specs
    )
    for i in range(num_of_rows):
        print(interpreter.interpret())
        if newline and i <= num_of_rows - 2:
            print()


if __name__ == '__main__':
    try:
        if len(sys.argv) != 3:
            raise_input_exception(
                "A Badger call has exactly two args."
            )
        path  = sys.argv[1]
        try:
            times = int(sys.argv[2])
        except ValueError as e:
            raise_input_exception(
                "The second argument of a Badger call is a positive integer."
            )

        file = open(path, "r")
        code  = file.read()
        file.close()

        #parse_and_print(code)
        #analyze_and_print(code)
        generate_and_print(code, times)
    except InputException as e:
        msg = e.args[0]["msg"]
        if "line" in e.args[0]:
            line = str(e.args[0]["line"])
            print("INPUT ERROR in line " + line + ": " + msg)
        else:
            print("INPUT ERROR: " + msg)
    except FileNotFoundError as e:
        print("INPUT ERROR: No such file or directory: " + e.filename)
    except Exception as e:
        print("ERROR: " + str(e))