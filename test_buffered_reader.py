# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import unittest
from buffered_reader import BufferedReader

class BufferedReaderTest(unittest.TestCase):

    EXPECTED_FILE_CONTENT = [
        "a", "b", "", "d", "e", "f", "g",
        "hijklmnop",
        "q", "r", "s", "t", "", "", "", "xyz"
    ]


    def test_br_with_defaults(self):
        result_default = []
        with BufferedReader("testres/test.file") as br:
            for item in br:
                result_default.append(item)
        self.assertTrue(
            result_default == self.EXPECTED_FILE_CONTENT
        )


    def test_br_with_bufsize_4(self):
        result_with_bufsize_4 = []
        br = BufferedReader("testres/test.file")
        br.bufsize = 4
        with br as br:
            for item in br:
                result_with_bufsize_4.append(item)
        self.assertTrue(
            result_with_bufsize_4 == self.EXPECTED_FILE_CONTENT
        )


    def test_br_with_bufsize_5(self):
        result_with_bufsize_5 = []
        br = BufferedReader("testres/test.file")
        br.bufsize = 5
        with br as br:
            for item in br:
                result_with_bufsize_5.append(item)
        self.assertTrue(
            result_with_bufsize_5 == self.EXPECTED_FILE_CONTENT
        )


    def test_br_with_bufsize_1(self):
        result_with_bufsize_1 = []
        br = BufferedReader("testres/test.file")
        br.bufsize = 2
        with br as br:
            for item in br:
                result_with_bufsize_1.append(item)
        self.assertTrue(
            result_with_bufsize_1 == self.EXPECTED_FILE_CONTENT
        )


#######################################


    # def test_br_with_multi_char_sep_and_default_bufsize(self):
    #     result_default = []
    #     with BufferedReader("testres/test.multicharsep.file", ", ") as br:
    #         for item in br:
    #             result_default.append(item)
    #     self.assertTrue(
    #         result_default == self.EXPECTED_FILE_CONTENT
    #     )


    # def test_br_with_multi_char_sep_and_bufsize_4(self):
    #     result_with_bufsize_4 = []
    #     br = BufferedReader("testres/test.multicharsep.file", ", ", 4)
    #     with br as br:
    #         for item in br:
    #             result_with_bufsize_4.append(item)
    #     self.assertTrue(
    #         result_with_bufsize_4 == self.EXPECTED_FILE_CONTENT
    #     )


    # def test_br_with_multi_char_sep_and_bufsize_5(self):
    #     result_with_bufsize_5 = []
    #     br = BufferedReader("testres/test.multicharsep.file", ", ", 5)
    #     with br as br:
    #         for item in br:
    #             result_with_bufsize_5.append(item)
    #     self.assertTrue(
    #         result_with_bufsize_5 == self.EXPECTED_FILE_CONTENT
    #     )


    # def test_br_with_multi_char_sep_and_bufsize_1(self):
    #     result_with_bufsize_3 = []
    #     br = BufferedReader("testres/test.multicharsep.file", ", ", 1)
    #     with br as br:
    #         for item in br:
    #             result_with_bufsize_1.append(item)
    #     self.assertTrue(
    #         result_with_bufsize_1 == self.EXPECTED_FILE_CONTENT
    #     )


#######################################


    # def test_br_with_multi_line_sep_and_default_bufsize(self):
    #     result_default = []
    #     with BufferedReader("testres/test.multilinesep.file", "\n***\n") as br:
    #         for item in br:
    #             result_default.append(item)
    #     self.assertTrue(
    #         result_default == self.EXPECTED_FILE_CONTENT
    #     )


    # def test_br_with_multi_line_sep_and_bufsize_4(self):
    #     result_with_bufsize_4 = []
    #     br = BufferedReader("testres/test.multilinesep.file", "\n***\n", 4)
    #     with br as br:
    #         for item in br:
    #             result_with_bufsize_4.append(item)
    #     self.assertTrue(
    #         result_with_bufsize_4 == self.EXPECTED_FILE_CONTENT
    #     )


    # def test_br_with_multi_line_sep_and_bufsize_5(self):
    #     result_with_bufsize_5 = []
    #     br = BufferedReader("testres/test.multilinesep.file", "\n***\n", 5)
    #     with br as br:
    #         for item in br:
    #             result_with_bufsize_5.append(item)
    #     self.assertTrue(
    #         result_with_bufsize_5 == self.EXPECTED_FILE_CONTENT
    #     )


#######################################


    # def test_br_with_whitespace_sep_and_default_bufsize(self):
    #     result_default = []
    #     with BufferedReader("testres/test.whitespacesep.file", "") as br:
    #         for item in br:
    #             result_default.append(item)
    #     self.assertTrue(
    #         result_default == [x for x in self.EXPECTED_FILE_CONTENT if x != ""]
    #     )


    # def test_br_with_whitespace_sep_and_bufsize_4(self):
    #     result_with_bufsize_4 = []
    #     br = BufferedReader("testres/test.whitespacesep.file", "", 4)
    #     with br as br:
    #         for item in br:
    #             result_with_bufsize_4.append(item)
    #     self.assertTrue(
    #         result_with_bufsize_4 == [x for x in self.EXPECTED_FILE_CONTENT if x != ""]
    #     )


    # def test_br_with_whitespace_sep_and_bufsize_5(self):
    #     result_with_bufsize_5 = []
    #     br = BufferedReader("testres/test.whitespacesep.file", "", 5)
    #     with br as br:
    #         for item in br:
    #             result_with_bufsize_5.append(item)
    #     self.assertTrue(
    #         result_with_bufsize_5 == [x for x in self.EXPECTED_FILE_CONTENT if x != ""]
    #     )


    # def test_br_with_whitespace_sep_and_bufsize_1(self):
    #     result_with_bufsize_1 = []
    #     br = BufferedReader("testres/test.whitespacesep.file", "", 1)
    #     with br as br:
    #         for item in br:
    #             result_with_bufsize_1.append(item)
    #     self.assertTrue(
    #         result_with_bufsize_1 == [x for x in self.EXPECTED_FILE_CONTENT if x != ""]
    #     )


if __name__ == '__main__':
    unittest.main()