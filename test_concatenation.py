# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import unittest
from main import generate
from helpers import resolve_docstring


class ConcatenationTest(unittest.TestCase):

    def test_simple_conc(self):
        sample = generate(
            "# s = \"bar\"\n# c = \"foo\" + s\n{c}",
            1
        )
        s = sample[0]
        self.assertTrue(s == "foobar")


    def test_long_conc(self):
        sample = generate(
            "# c = \"foo\" + \"bar\" + \"baz\" + \"lol\"\n{c}",
            1
        )
        s = sample[0]
        self.assertTrue(s == "foobarbazlol")


    def test_nested_conc(self):
        code = """
            # MyInt = int min=0 max=9
            # c = {\"foo\" | \"bar\"} + {MyInt + \"baz\"}
            #
            {c}
        """
        sample = generate(resolve_docstring(code), 10)
        self.assertTrue(all(map(
            lambda item: (
                (item[0:3] == "foo" or item[0:3] == "bar")
                and item[-3:] == "baz"
                and item[3:-3].isdigit()
            ), 
            sample
        )))


if __name__ == '__main__':
    unittest.main()