# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import unittest
from helpers import is_decimal_with_n_places, resolve_docstring
from input_exception import InputException
from math import floor
from main import generate
from number_sampling import *
from hist import NaturalCenteredHist, TrivialHist


class DecimalTest(unittest.TestCase):

    def test_decimal_raw(self):
        sample = generate(
            "# i = decimal\n{i}",
            100_000
        )
        hist = NaturalCenteredHist(sample, -30, 30, 0)
        self.assertTrue(hist.freq_at_is_between(0, 3500, 4500))
        self.assertTrue(hist.freqs_at_are_between([-20, +20], 300, 700))
        self.assertTrue(hist.diffs_between_are_between(-20, -10, 0, 500))
        self.assertTrue(hist.diffs_between_are_between(+10, +20, -500, 0))
        self.assertTrue(hist.freqs_between_are_between(-10, 0, 2000, 4500))
        self.assertTrue(hist.freqs_between_are_between(0, 10, 2000, 4500))
        self.assertTrue(hist.freqs_between_are_between(-30, -20, 25, 750))
        self.assertTrue(hist.freqs_between_are_between(20, 30, 25, 750))
        self.assertTrue(hist.left_out_is_between(50,150))
        self.assertTrue(hist.right_out_is_between(50,150))


    def test_decimal_with_min_equals_max(self):
        sample = generate(
            "# i = decimal min=42 max=42\n{i}",
            1
        )
        n = float(sample[0])
        self.assertTrue(n == 42)

        sample = generate(
            "# i = decimal min=42 max=42 dist=desc\n{i}",
            1
        )
        n = float(sample[0])
        self.assertTrue(n == 42)

        sample = generate(
            "# i = decimal min=42 max=42 dist=asc\n{i}",
            1
        )
        n = float(sample[0])
        self.assertTrue(n == 42)


    def test_decimal_with_max_smaller_than_min(self):
        with self.assertRaises(InputException):
            generate(
                "# i = decimal min=0 max=-42\n{i}",
                1
            )
        with self.assertRaises(InputException):
            generate(
                "# i = decimal min=0 max=-42 dist=desc\n{i}",
                1
            )
        with self.assertRaises(InputException):
            generate(
                "# i = decimal min=0 max=-42 dist=asc\n{i}",
                1
            )
        with self.assertRaises(InputException):
            generate(
                "# i = decimal min=0 max=-42 dist=normal\n{i}",
                1
            )
        with self.assertRaises(InputException):
            generate(
                "# i = decimal min=0 max=-42 dist=pareto\n{i}",
                1
            )


    def test_decimal_with_alternative_notation(self):
        sample = generate(
            "# i = decimal min=0 max=10 dist=DESC\n{i}",
            1000
        )
        hist = TrivialHist(sample, 0, 10, 10)
        self.assertTrue(hist.freq_at_is_between(0, 150, 250))
        self.assertTrue(hist.diffs_between_are_between(0, 10, -300, 30))


    def test_decimal_with_invalid_min_raises_exception(self):
        code = resolve_docstring("""
            # MyDec = decimal min=foo
            {MyDec}
        """)
        with self.assertRaises(InputException):
            generate(code, 1)


###


    def test_decimal_uniform(self):
        sample = generate(
            "# i = decimal min=0 max=10\n{i}",
            100_000
        )
        hist = TrivialHist(sample, 0, 10, 100)
        self.assertTrue(hist.freqs_between_are_between(0, 10, 800, 1200))
        self.assertTrue(hist.left_out_is_between(0,0))
        self.assertTrue(hist.right_out_is_between(0,0))
        

    def test_decimal_with_neg_max_only(self):
        with self.assertRaises(InputException):
            generate(
                "# i = decimal max=-200\n{i}",
                1
            )


    def test_decimal_uniform_with_mu(self):
        sample = generate(
            "# i = decimal min=0 max=10 mu=42\n{i}",
            1000
        )
        hist = TrivialHist(sample, 0, 10, 10)
        self.assertTrue(hist.freqs_between_are_between(0, 10, 70, 130))
        self.assertTrue(hist.left_out_is_between(0,0))
        self.assertTrue(hist.right_out_is_between(0,0))


    def test_decimal_uniform_with_precision_2(self):
        sample = generate(
            "# i = decimal min=0 max=10 mu=42 precision=2\n{i}",
            100_000
        )
        self.assertTrue(is_decimal_with_n_places(sample, 2))
        hist = NaturalCenteredHist(sample, 0, 10, 2)
        self.assertTrue(hist.freqs_between_are_between(0, 10, 60, 140))
        self.assertTrue(hist.left_out_is_between(0,0))
        self.assertTrue(hist.right_out_is_between(0,0))


    def test_decimal_uniform_with_precision_0(self):
        sample = generate(
            "# i = decimal min=0 max=10 mu=42 precision=0\n{i}",
            1000
        )
        self.assertTrue(is_decimal_with_n_places(sample, 0))
        hist = NaturalCenteredHist(sample, 0, 10, 0)
        self.assertTrue(hist.freqs_between_are_between(0, 10, 60, 120))
        self.assertTrue(hist.left_out_is_between(0,0))
        self.assertTrue(hist.right_out_is_between(0,0))


###


    def test_decimal_with_desc(self):
        sample = generate(
            "# i = decimal min=0 max=10 dist=desc\n{i}",
            100_000
        )
        hist = TrivialHist(sample, 0, 10, 100)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(0, 1900, 2100))        
        self.assertTrue(hist.diffs_between_are_between(0, 10, -300, 200))
        self.assertTrue(hist.freq_at_is_between(10, 0, 20))
        self.assertTrue(hist.right_out() == 0)


    def test_decimal_with_asc(self):
        sample = generate(
            "# i = decimal min=0 max=10 dist=asc\n{i}",
            100_000
        )
        hist = TrivialHist(sample, 0, 10, 100)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(0, 0, 50))        
        self.assertTrue(hist.diffs_between_are_between(0, 10, -200, 300))
        self.assertTrue(hist.freq_at_is_between(10, 1900, 2100))
        self.assertTrue(hist.right_out() == 0)


    def test_decimal_with_desc_and_only_one_bound(self):
        with self.assertRaises(InputException):
            generate(
                "# i = decimal max=-200 dist=desc\n{i}",
                1
            )


    def test_decimal_with_desc_dist_and_precision_2(self):
        sample = generate(
            "# i = decimal min=0 max=10 dist=desc precision=2\n{i}",
            1000
        )
        self.assertTrue(is_decimal_with_n_places(sample, 2))
        hist = TrivialHist(sample, 0, 10, 10)
        self.assertTrue(hist.freq_at_is_between(0, 125, 250))
        self.assertTrue(hist.diffs_between_are_between(0, 10, -120, 40))


    def test_decimal_with_desc_dist_and_precision_0(self):
        sample = generate(
            "# i = decimal min=0 max=10 dist=desc precision=0\n{i}",
            1000
        )
        self.assertTrue(is_decimal_with_n_places(sample, 0))
        hist = TrivialHist(sample, 0, 10, 10)
        self.assertTrue(hist.freq_at_is_between(0, 125, 250))
        self.assertTrue(hist.diffs_between_are_between(0, 10, -120, 40))


###


    def test_decimal_with_normal_dist_and_custom_mu(self):
        sample = generate(
            "# i = decimal dist=normal mu=-42.5\n{i}",
            100_000
        )
        hist = NaturalCenteredHist(sample, -72.5, -12.5, 0)
        self.assertTrue(hist.freq_at_is_between(-42.5, 3500, 4500))
        self.assertTrue(hist.left_out_is_between(1, 200))
        self.assertTrue(hist.right_out_is_between(1, 200))
        self.assertTrue(hist.freqs_between_are_between(-50, -42.5, 2500, 4500))
        self.assertTrue(hist.freqs_between_are_between(-42.5, -35, 2500, 4500))
        self.assertTrue(hist.diffs_between_are_between(-65, -50, 0, 500))
        self.assertTrue(hist.diffs_between_are_between(-35, -20, -500, 0))
        self.assertTrue(hist.freqs_between_are_between(-20, -12.5, 0, 600))
        self.assertTrue(hist.freqs_between_are_between(-72.5, -65, 0, 600))


    def test_decimal_with_normal_dist_and_custom_sigma(self):
        sample = generate(
            "# i = decimal dist=normal sigma=4.2\n{i}",
            100_000
        )
        hist = NaturalCenteredHist(sample, -15, +15, 1)
        self.assertTrue(hist.freq_at_is_between(0, 800, 1100))
        self.assertTrue(hist.left_out_is_between(1, 200))
        self.assertTrue(hist.right_out_is_between(1, 200))
        self.assertTrue(hist.freqs_between_are_between(-2, +2, 700, 1100))
        self.assertTrue(hist.diffs_between_are_between(-10, -2, -200, 400))
        self.assertTrue(hist.diffs_between_are_between(+2, +10, -400, 200))
        self.assertTrue(hist.freqs_between_are_between(-15, -10, 0, 100))
        self.assertTrue(hist.freqs_between_are_between(+10, -15, 0, 100))


    def test_decimal_with_normal_dist_and_bound_specs_and_custom_mu(self):
        sample = generate(
            "# i = decimal dist=normal mu=-42.5 min=-60\n{i}",
            100_000
        )
        hist = NaturalCenteredHist(sample, -60, -10, 0)
        self.assertTrue(hist.left_out_is_between(0, 0))
        self.assertTrue(hist.freq_at_is_between(-60, 4000, 5000))
        self.assertTrue(hist.diffs_between_are_between(-59, -49, -100, 500))
        self.assertTrue(hist.freqs_between_are_between(-49, -37, 3000, 5000))
        self.assertTrue(hist.diffs_between_are_between(-37, -25, -500, 100))
        self.assertTrue(hist.freqs_between_are_between(-25, -10, 0, 1000))
        self.assertTrue(hist.right_out_is_between(1, 100))


    def test_decimal_with_normal_dist_and_bound_specs_without_mu(self):
        sample = generate(
            "# i = decimal dist=normal min=-22 max=42\n{i}",
            100_000
        )
        hist = NaturalCenteredHist(sample, -20, 40)
        self.assertTrue(hist.freq_at_is_between(10, 3500, 4500))
        self.assertTrue(hist.freqs_at_are_between([-10, +30], 300, 700))
        self.assertTrue(hist.diffs_between_are_between(-10, 0, 0, 500))
        self.assertTrue(hist.diffs_between_are_between(+20, +30, -500, 0))
        self.assertTrue(hist.freqs_between_are_between(0, 10, 2000, 4500))
        self.assertTrue(hist.freqs_between_are_between(10, 20, 2000, 4500))
        self.assertTrue(hist.freqs_between_are_between(-20, -10, 25, 750))
        self.assertTrue(hist.freqs_between_are_between(30, 40, 25, 750))
        self.assertTrue(hist.left_out_is_between(50, 150))
        self.assertTrue(hist.right_out_is_between(50, 150))


    def test_decimal_with_normal_dist_and_min_close_to_mu(self):
        with self.assertRaises(InputException):
            generate(
                "# i = decimal dist=normal min=0 mu=0\n{i}",
                1
            )


###


    def test_decimal_with_pareto_dist(self):
        sample = generate(
            "# i = decimal dist=pareto\n{i}",
            100_000
        )
        hist = TrivialHist(sample, 0, 10, 100)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(0, 8000, 10_000))        
        self.assertTrue(hist.diffs_between_are_between(0, 1, -4000, -100))
        self.assertTrue(hist.freqs_between_are_between(1, 3, 500, 3000))
        self.assertTrue(hist.freqs_between_are_between(3, 10, 50, 1000))
        self.assertTrue(hist.freq_at_is_between(10, 50, 150))


    def test_decimal_with_pareto_dist_and_custom_factor_and_shifted(self):
        sample = generate(
            "# i = decimal dist=pareto min=-100 paretofactor=10\n{i}",
            100_000
        )
        hist = TrivialHist(sample, -100, 0, 100)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(-100, 8000, 10_000))        
        self.assertTrue(hist.diffs_between_are_between(-100, -90, -4000, -100))
        self.assertTrue(hist.freqs_between_are_between(-90, -80, 800, 3000))
        self.assertTrue(hist.freqs_between_are_between(-80, -50, 100, 1500))
        self.assertTrue(hist.freqs_between_are_between(-50, -100, 50, 500))
        self.assertTrue(hist.freq_at_is_between(0, 50, 150))


    def test_decimal_with_pareto_dist_and_custom_shape_parameter(self):
        sample = generate(
            "# i = decimal dist=pareto paretoshape=2\n{i}",
            100_000
        )
        hist = TrivialHist(sample, 0, 10)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(0, 15000, 20000))        
        self.assertTrue(hist.diffs_between_are_between(0, 1/2, -5000, -500))
        self.assertTrue(hist.freqs_between_are_between(1/2, 2, 500, 7500))
        self.assertTrue(hist.freqs_between_are_between(2, 5, 50, 1000))
        self.assertTrue(hist.freqs_between_are_between(5, 10, 0, 200))


    def test_decimal_with_pareto_dist(self):
        sample = generate(
            "# i = decimal dist=pareto min=0 max=10\n{i}",
            100_000
        )
        hist = TrivialHist(sample, 0, 10, 100)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(0, 8000, 10_000))        
        self.assertTrue(hist.diffs_between_are_between(0, 1, -4000, -100))
        self.assertTrue(hist.freqs_between_are_between(1, 3, 500, 3000))
        self.assertTrue(hist.freqs_between_are_between(3, 9.8, 50, 1000))
        self.assertTrue(hist.freq_at_is_between(10, 8000, 12_000))


if __name__ == '__main__':
    unittest.main()
    #dt = DecimalTest()
    #dt.test_decimal_with_pareto_dist_and_custom_shape_parameter()
