# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import unittest
from os import popen
from helpers import is_int, resolve_docstring
from main import *
from interpret import BLINDTEXT


class MiscTest(unittest.TestCase):

    def test_blindtext_raw(self):
        sample = generate(
            "# b = blindtext\n{b}",
            1
        )
        self.assertTrue(sample[0], BLINDTEXT)


    def test_main(self):
        with popen("python3 main.py testres/test.schema 1") as out:
            result = out.read()
        self.assertTrue(result in ["foo\n", "bar\n"])


    def test_raw_body(self):
        sample = generate(
            "{int}",
            1
        )
        self.assertTrue(is_int(sample[0]))


    def test_functioning_of_inverse_var_def(self):
        code = resolve_docstring("""
            # bar = foo + "2"
            # foo = "4"
            Some {bar}.
        """)
        result = generate(code, 1)[0]
        self.assertTrue(result == "Some 42.")
            

    def test_exception_on_cycle(self):
        code = resolve_docstring("""
            # x = y
            # y = x
            Some text.
        """)
        with self.assertRaises(InputException):
            generate(code, 1)


    def test_exception_on_more_complex_cycle(self):
        code = resolve_docstring("""
            # foo = int
            # start = foo | bar
            # bar = baz + "42"
            # baz = start + "42"
            Some text.
        """)
        with self.assertRaises(InputException):
            generate(code, 1)


    def test_exception_on_undefined_var_in_head(self):
        code = resolve_docstring("""
            # foo = bar
            Some text.
        """)
        with self.assertRaises(InputException):
            generate(code, 1)


    def test_exception_on_undefined_var_in_body(self):
        code = resolve_docstring("""
            # foo = int
            Some {bar} text.
        """)
        with self.assertRaises(InputException):
            generate(code, 1)


    def test_exception_on_illegal_func_call_in_head(self):
        code = resolve_docstring("""
            # concat = function {$0 + $1}
            # foo = concat + "bar"
            Some text.
        """)
        with self.assertRaises(InputException):
            generate(code, 1)


    def test_exception_on_illegal_func_call_in_body(self):
        code = resolve_docstring("""
            # concat = function {$0 + $1}
            Some {concat} text.
        """)
        with self.assertRaises(InputException):
            generate(code, 1)


if __name__ == '__main__':
    unittest.main()
