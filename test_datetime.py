# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import unittest
from main import generate
from input_exception import InputException
from helpers import resolve_docstring


class DatetimeTest(unittest.TestCase):

    def test_date_raw(self):
        sample = generate(
            "# i = date\n{i}",
            1
        )
        self.assertRegex(sample[0],"\\d{4,4}-\\d\\d-\\d\\d")


    def test_date_with_full_spec(self):
        code = resolve_docstring(
            """
            # D = date min=1.12.1999 max=31.03.2100 format="d. M yyyy"
            {D}
            """
        )
        sample = generate(code, 1)[0]
        self.assertRegex(sample, "\\d{1,2}\\. [A-Za-z]+ \\d{4,4}")


    def test_date_with_only_min(self):
        sample = generate(
            "# i = date min=1999-12-1\n{i}",
            1
        )
        self.assertRegex(sample[0],"\\d{4,4}-\\d\\d-\\d\\d")


    def test_date_with_german_min(self):
        sample = generate(
            "# i = date min=1.12.1999\n{i}",
            1
        )
        self.assertRegex(sample[0],"\\d{4,4}-\\d\\d-\\d\\d")


    def test_date_with_illegal_min_raises_exception(self):
        code = resolve_docstring("""
            # D = date min=foo
            {D}
        """)
        with self.assertRaises(InputException):
            generate(code, 1)


    def test_date_with_illegal_max_raises_exception(self):
        code = resolve_docstring("""
            # D = date max=foo
            {D}
        """)
        with self.assertRaises(InputException):
            generate(code, 1)


    def test_date_with_illegal_output_format_raises_exception(self):
        code = resolve_docstring("""
            # D = date format=foo
            {D}
        """)
        with self.assertRaises(InputException):
            generate(code, 1)


if __name__ == '__main__':
    unittest.main()