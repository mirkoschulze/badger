# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from analyze import (
    VARIABLE,
    STRING_LITERAL,
    BODY_TEXT,
    SIMPLE_EMBEDDING,
    ARRAY_EMBEDDING
)
from helpers import (
    enquote_and_escape,
    resolve_docstring,
    resolve_escaped_string
)
from input_exception import raise_input_exception

from datetime_sampling import random_datetime
from list_sampling import *
from number_sampling import *

from datetime import datetime, timedelta
from math import ceil, floor, sqrt
from random import uniform, randint, randrange,\
    normalvariate, paretovariate

import exrex


BLINDTEXT = resolve_docstring(
    """
    Hello, here is some text without a meaning.
    This text should show what a printed text will look like at this place.
    """,
    ignore_linebreaks=True
)


class Interpreter:

    def __init__(self, body, individuals, num_of_rows, var_specs):
        self.body = body
        self.individuals = individuals
        self.local_mem = {}
        self.num_of_rows = num_of_rows
        self.var_specs = var_specs


    def interpret(self):
        output = ""
        for unit in self.body:
            unit_type = unit[0]
            if unit_type == BODY_TEXT:
                output += unit[1]
            elif unit_type == SIMPLE_EMBEDDING:
                output += self.interpret_simple_embedding(unit[1])
            elif unit_type == ARRAY_EMBEDDING:
                output += self.interpret_array_embedding(unit[1])
            else:
                raise Exception("Internal error during interpretation.")
        return output


    def interpret_simple_embedding(self, identifier):
        result = self.interpret_variable(identifier)
        if self.var_specs[identifier]["type"] == "array":
            return "".join(result)
        return result


    def interpret_array_embedding(self, spec):
        identifiers = spec["identifiers"]
        delim = spec["delim"]
        values = []
        for identifier in identifiers:
            interpretation_result = self.interpret_variable(identifier)
            if self.var_specs[identifier]["type"] == "array":
                values += interpretation_result
            else:
                values.append(interpretation_result)
        # vvv generate output vvv
        i = 0
        while i < len(values):
            if values[i] != "":
                break
            i += 1
        if i == len(values):
            return ""
        output = values[i]
        i += 1
        while i < len(values):
            value = values[i]
            if value != "":
                output += delim
                output += value
            i += 1
        return output


    def interpret_variable(self, identifier):
        assert identifier in self.var_specs
        if self.var_specs[identifier]["type"] == "alternation":
            return self.interpret_alternation(identifier)
        if identifier in self.local_mem:
            return self.local_mem[identifier]
        result = None
        if self.var_specs[identifier]["type"] == "array":
            result = self.interpret_array(identifier)
        else:
            result = self.interpret_expr(self.var_specs[identifier])
        if identifier in self.individuals:
            self.local_mem[identifier] = result
        return result


    def interpret_expr(self, type_spec):
        if type_spec["type"] == VARIABLE:
            return self.interpret_variable(type_spec["target"] )
        elif type_spec["type"] == STRING_LITERAL:
            return type_spec["string"]
        elif type_spec["type"] == "blindtext":
            return BLINDTEXT
        elif type_spec["type"] == "int":
            return self.interpret_int(type_spec)
        elif type_spec["type"] == "decimal":
            return self.interpret_decimal(type_spec)
        elif type_spec["type"] == "date":
            return self.interpret_date(type_spec)
        elif type_spec["type"] == "regex":
            return self.interpret_regex(type_spec)
        elif type_spec["type"] == "list_import":
            return self.interpret_list_import(type_spec)
        elif type_spec["type"] == "schema_import":
            return self.interpret_schema_import(type_spec)
        elif type_spec["type"] == "concatenation":
            return self.interpret_concatenation(type_spec)
        elif type_spec["type"] == "union":
            return self.interpret_union(type_spec)
        elif type_spec["type"] == "evaluation":
            return self.interpret_evaluation(type_spec)
        else:
            raise Exception(
                "Interpretation of expression representation incomplete. Repr: "
                + str(type_spec)
            )


    def interpret_int(self, type_spec):
        dist = type_spec["dist"]
        minimum = type_spec["min"] if "min" in type_spec else None
        maximum = type_spec["max"] if "max" in type_spec else None

        if dist == "uniform":
            return str(randint(minimum, maximum))
        if dist == "desc":
            return str(draw_int_with_desc_probability(
                minimum,
                maximum
            ))
        if dist == "asc":
            return str(draw_int_with_asc_probability(
                minimum,
                maximum
            ))
        if dist == "normal":
            result = normalvariate(type_spec["mu"], type_spec["sigma"])
            if minimum is not None and result < minimum:
                result = minimum
            if maximum is not None and result > maximum:
                result = maximum
            return str(int(round(result, 0)))
        if dist == "pareto":
            result = draw_paretovariate(
                minimum,
                type_spec["paretoshape"],
                type_spec["paretofactor"]
            )
            if maximum is not None and result > maximum:
                result = maximum
            return str(int(round(result, 0)))
        raise Exception("Number interpretation is not complete.")


    def interpret_decimal(self, type_spec):
        dist = type_spec["dist"]
        precision = type_spec["precision"]
        minimum = type_spec["min"] if "min" in type_spec else None
        maximum = type_spec["max"] if "max" in type_spec else None

        if dist == "uniform":
            return str(round(uniform(minimum, maximum), precision))
        if dist == "desc":
            return str(round(
                draw_decimal_with_desc_probability(
                    minimum,
                    maximum
                ),
                precision
            ))
        if dist == "asc":
            return str(round(
                draw_decimal_with_asc_probability(
                    minimum,
                    maximum
                ),
                precision
            ))
        if dist == "normal":
            result = normalvariate(type_spec["mu"], type_spec["sigma"])
            if minimum is not None and result < minimum:
                result = minimum
            if maximum is not None and result > maximum:
                result = maximum
            return str(round(result, precision))
        if dist == "pareto":
            result = draw_paretovariate(
                minimum,
                type_spec["paretoshape"],
                type_spec["paretofactor"]
            )
            if maximum is not None and result > maximum:
                result = maximum
            return str(round(result, precision))
        raise Exception("Number interpretation is not complete.")


    def interpret_evaluation(self, type_spec):
        func = type_spec["function"]
        code = self.var_specs[func]["code"]
        input_modifiers = type_spec["input_modifiers"]
        output_modifiers = type_spec["output_modifiers"]
        args_raw = type_spec["arguments"]
        args = []
        for i in range(len(args_raw)):
            value = self.interpret_expr(args_raw[i])
            if value in output_modifiers:
                return output_modifiers[value]
            if value in input_modifiers:
                value = input_modifiers[value]
            code = code.replace("$" + str(i), enquote_and_escape(value))
            args.append(value)
        code = code.replace("$args", str(args)) # TODO!!!
        return str(eval(code))


    def interpret_date(self, type_spec):
        minimum = type_spec["min"]
        maximum = type_spec["max"]
        format_str = type_spec["format"]
        return random_datetime(minimum, maximum).strftime(format_str)


    def interpret_regex(self, type_spec):
        return exrex.getone(type_spec["regex"])


    def interpret_list_import(self, type_spec):
        if len(type_spec["cache"]) == 0:
            source = type_spec["source"]
            sep = type_spec["sep"]
            dist = type_spec["dist"]
            if dist == "uniform":
                type_spec["cache"] = sample_from_file_uniform(source, sep, self.num_of_rows)
            if dist == "desc":
                type_spec["cache"] = sample_from_file_desc(source, sep, self.num_of_rows)
            if dist == "asc":
                type_spec["cache"] = sample_from_file_asc(source, sep, self.num_of_rows)
            if dist == "natural":
                type_spec["cache"] = read_cyclic_from_file(source, sep, self.num_of_rows)
            if dist == "pareto":
                type_spec["cache"] = sample_from_file_paretovariate(
                    source,
                    sep,
                    self.num_of_rows,
                    type_spec["paretoshape"],
                    type_spec["paretofactor"]
                )
        return type_spec["cache"].pop(0)


    def interpret_schema_import(self, type_spec):
        if len(type_spec["cache"]) == 0:
            other_interpreter = Interpreter(
                type_spec["body"],
                type_spec["individuals"],
                self.num_of_rows,
                type_spec["var_specs"],
            )
            for i in range(self.num_of_rows):
                type_spec["cache"].append(
                    other_interpreter.interpret()
                )
        return type_spec["cache"].pop(0)


    def interpret_concatenation(self, type_spec):
        return "".join(
            [self.interpret_expr(target) for target in type_spec["targets"]]
        )


    def interpret_union(self, type_spec):
        targets = type_spec["targets"]
        freqs = type_spec["freqs"]
        index_drawn = draw_by_proportion(freqs)
        return self.interpret_expr(targets[index_drawn])


    def interpret_alternation(self, identifier):
        if identifier in self.local_mem:
            return self.interpret_variable(self.local_mem[identifier])
        targets = self.var_specs[identifier]["targets"]
        freqs = self.var_specs[identifier]["freqs"]
        index_drawn = draw_by_proportion(freqs)
        self.local_mem[identifier] = targets[index_drawn]["target"]
        return self.interpret_expr(targets[index_drawn])


    def interpret_array(self, identifier):
        spec = self.var_specs[identifier]
        output = []
        if spec["subtype"] == "sub":
            from_index = spec["from"]
            to_index = spec["to"] + 1
            return self.interpret_variable(spec["target"])[from_index:to_index]
        for i in range(spec["length"]):
            output.append(self.interpret_expr(spec["target"]))
        return output