# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import unittest
from hist import NaturalCenteredHist, TrivialHist
from number_sampling import (
    draw_int_with_desc_probability,
    draw_int_with_asc_probability,
    draw_decimal_with_desc_probability,
    draw_decimal_with_asc_probability,
    draw_paretovariate,
)
from helpers import shift_lower_bound_to_conform_to_precision


class NumberSamplingTest(unittest.TestCase):

    def test_draw_decimal_with_desc_probability(self):
        sample = [
            draw_decimal_with_desc_probability(0, 10)
            for i in range(100_000)
        ]
        hist = TrivialHist(sample, 0, 10, 100)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(0, 1900, 2100))        
        self.assertTrue(hist.diffs_between_are_between(0, 10, -300, 200))
        self.assertTrue(hist.freq_at_is_between(10, 0, 20))
        self.assertTrue(hist.right_out() == 0)


    def test_draw_decimal_with_asc_probability(self):
        sample = [
            draw_decimal_with_asc_probability(0, 10)
            for i in range(100_000)
        ]
        hist = TrivialHist(sample, 0, 10, 100)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(0, 0, 20))        
        self.assertTrue(hist.diffs_between_are_between(0, 10, -200, 300))
        self.assertTrue(hist.freq_at_is_between(10, 1900, 2100))
        self.assertTrue(hist.right_out() == 0)


    def test_draw_int_with_desc_probability(self):
        sample = [
            draw_int_with_desc_probability(0, 10)
            for i in range(100_000)
        ]
        hist = NaturalCenteredHist(sample, 0, 10, 1)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(0, 16000, 18000))
        self.assertTrue(hist.freqs_at_are_between([1,2,3,4,5,6,7,8,9], 2000, 16000))     
        self.assertTrue(hist.freqs_between_are_between(0.1, 0.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(1.1, 1.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(2.1, 2.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(3.1, 3.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(4.1, 4.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(5.1, 5.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(6.1, 6.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(7.1, 7.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(8.1, 8.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(9.1, 9.9, 0, 0))
        self.assertTrue(hist.freq_at_is_between(10, 1000, 2000))
        self.assertTrue(hist.right_out() == 0)


    def test_draw_int_with_asc_probability(self):
        sample = [
            draw_int_with_asc_probability(0, 10)
            for i in range(100_000)
        ]
        hist = NaturalCenteredHist(sample, 0, 10, 1)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(0, 1000, 2000))
        self.assertTrue(hist.freqs_at_are_between([1,2,3,4,5,6,7,8,9], 2000, 16000))     
        self.assertTrue(hist.freqs_between_are_between(0.1, 0.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(1.1, 1.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(2.1, 2.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(3.1, 3.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(4.1, 4.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(5.1, 5.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(6.1, 6.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(7.1, 7.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(8.1, 8.9, 0, 0))
        self.assertTrue(hist.freqs_between_are_between(9.1, 9.9, 0, 0))
        self.assertTrue(hist.freq_at_is_between(10, 16000, 18000))
        self.assertTrue(hist.right_out() == 0)


    def test_draw_pareto_with_defaults(self):
        sample = [
            draw_paretovariate(0, 1, 1)
            for i in range(100_000)
        ]
        hist = TrivialHist(sample, 0, 10, 100)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(0, 8000, 10_000))        
        self.assertTrue(hist.diffs_between_are_between(0, 1, -4000, -100))
        self.assertTrue(hist.freqs_between_are_between(1, 3, 500, 3000))
        self.assertTrue(hist.freqs_between_are_between(3, 10, 50, 1000))
        self.assertTrue(hist.freq_at_is_between(10, 50, 150))


    def test_draw_pareto_with_factor_and_shifted(self):
        sample = [
            draw_paretovariate(-100, 1, 10)
            for i in range(100_000)
        ]
        hist = TrivialHist(sample, -100, 0, 100)
        self.assertTrue(hist.left_out() == 0)
        self.assertTrue(hist.freq_at_is_between(-100, 8000, 10_000))        
        self.assertTrue(hist.diffs_between_are_between(-100, -95, -4000, 0))
        self.assertTrue(hist.freqs_between_are_between(-95, -80, 500, 5000))
        self.assertTrue(hist.freqs_between_are_between(-80, -50, 100, 1500))
        self.assertTrue(hist.freqs_between_are_between(-50, -100, 50, 500))
        self.assertTrue(hist.freq_at_is_between(0, 50, 150))


if __name__ == '__main__':
    unittest.main()