# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

class BufferedReader:

    bufsize = None # number of chars
    carry = None
    file = None
    items = None
    path = None
    separator = None
    

    def __init__(self, path, separator="\n", bufsize=1000):
        self.carry = ""
        self.items = []
        self.path = path
        self.separator = separator
        self.bufsize = bufsize

        if len(separator) != 1:
            raise ValueError(
                "Only one char separators supported yet."
            )
        if bufsize <= len(separator):
            raise ValueError(
                "Bufsize must be bigger than the length of the separator."
            )


    def __enter__(self):
        self.file = open(self.path, "r")
        return self


    def __iter__(self):
        return self


    def __next__(self):
        items = self.items
        if len(items) > 0:
            return items.pop(0)
        while True:
            if not self._read():
                if len(items) > 0:
                    return items.pop(0)
                raise StopIteration
            if len(items) > 0:
                return items.pop(0)


    def _read(self):
        if self.carry is None:
            return False
        content = self.file.read(self.bufsize)
        if not content:
            self.items.append(self.carry)
            self.carry = None
            return False
        split = []
        if self.separator == "":
            split = content.split()
        else:
            split = content.split(self.separator)
        if len(split) == 0:
            self.items.append(self.carry)
            self.carry = None
            return False            
        split[0] = self.carry + split[0]
        self.carry = split[-1]
        self.items += split[:-1]
        return True


    def __exit__(self, exception_type, exception_value, exception_traceback):
        self.file.close()


if __name__ == '__main__':
    print("a\n\n\n\nb".split("\n\n"))
    #timeout = 0
    #with BufferedReader("test.file", "\n", 4) as br:
    #    for item in br:
    #        print(item)
    #        timeout += 1
    #        if timeout > 1000:
    #            exit()