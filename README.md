# Badger

A Text-Based Data Generator




## What is Badger?

Note that Badger is only a personal proof of concept and not actively maintained!

Badger is a text-based data generator. You can specify a schema and generate any number of random data records from it. Since a Badger schema is a text template with embedded data specifications, it is not tied to a specific data format.
Badger is designed as a command line tool for Linux systems.





## Dependencies

My appreciation goes to the creators of the dependencies of Badger:

* [python 3](https://www.python.org/)
* [lark](https://pypi.org/project/lark/)
* [exrex](https://pypi.org/project/exrex/)




## Getting Started

### Installation

1) Install
    * [python 3](https://www.python.org/)
    * [lark](https://pypi.org/project/lark/)
    * [exrex](https://pypi.org/project/exrex/)
2) Clone the repository or download the project directory.
3) Execute `python3 main.py schemas/name.schema 3`.


### The First Schema

A schema is divided into head and body:
```
# variable = some definition ...
#
body with {variable}
```

The body is the skeleton of each data record. It is text enriched with variables that represent data types and values. Variables are separated from the text by braces.

The head is the area at the beginning of the schema. Each line is started by a hash character and contains either nothing or a variable definition. Each variable definition assigns a data type (aka. set of strings) or a concrete value to a variable that can be used in the body. A simple schema looks like this:
```
# MyInt = int min=0 max=42
#
My number of the day: {MyInt}
```

In this example, a custom data type is defined and assigned to the variable `MyInt`. This variable is then embedded in the body to describe a simple data row. That is all you need to know to write your first schema. However, the next section provides more details on the schema language.




## Usage Documentation

This section introduces the various features of Badger step by step. Before we get started, there are a number of important remarks about the language.

**Escaping**. The characters `\`, `{`,`}` and `"` need to be escaped in body text. This is done by a backslash. To make it clear: a left brace can be typed by `\{` and a backslash by `\\`.

**Variables**. Each variable can only be defined once. A variable must be alphanumeric, with the characters `_` and `'` also being permitted. A variable cannot start with a digit. Some keywords are reserved and cannot be used as variables. Variables are case sensitive.

**Other Syntax Details**. Some spaces are mandatory. Have a look at the following example schema:
```
# MyInt = int min=0 max=42
#
My number of the day: {MyInt}
```
The spaces around the first equality sign are mandatory. The space between `int` and `min` is mandatory. The space between `0` and `max` is mandatory. The absence of spaces around the second and third equality sign is mandatory. However, mandatory spaces do not have to be single spaces.


### Integers

Usage:

```
# MyInt = int ...options...
#
{MyInt}
```

Summary of Options:

|Option|Format|Description|
|---|---|---|
|min|integer|lower bound of the domain; inclusive|
|max|integer|upper bound of the domain; inclusive|
|dist|uniform, desc, asc, normal, pareto|specifies the probability distribution used to draw from the domain|
|mu_option|min\<float\<max|parameter used to customize the probability distribution if dist=normal|
|sigma_option|positive float|parameter used to customize the probability distribution if dist=normal; default is 10|
|paretoshape_option|positive float|parameter used to customize the probability distribution if dist=pareto|
|paretofactor_option|positive float|parameter used to customize the probability distribution if dist=pareto|

Remarks:

* If no option is specified, the integers are drawn normalvariate around zero.
* If only bounds are specified, the integers are drawn uniform.
* If *normal* is selected as distribution and the mu option is not explicitely set, mu is either set to zero or to the center of the bounds.
* If *pareto* is selected as distribution and there is no lower bound specified, the bound is set to zero.
* If *normal* or *pareto* is selected as distribution, the specified bounds may result in clipping behavior.


### Decimals

Usage:

```
# MyDecimal = decimal ...options...
#
{MyDecimal}
```

Summary of Options:

|Option|Format|Description|
|---|---|---|
|min|integer|lower bound of the domain; inclusive|
|max|integer|upper bound of the domain; inclusive|
|dist|uniform, desc, asc, normal, pareto|specifies the probability distribution used to draw from the domain|
|mu_option|min\<float\<max|parameter used to customize the probability distribution if dist=normal|
|sigma_option|positive float|parameter used to customize the probability distribution if dist=normal|
|paretoshape_option|positive float|parameter used to customize the probability distribution if dist=pareto|
|paretofactor_option|positive float|parameter used to customize the probability distribution if dist=pareto|
|precision|positive integer|maximum number of decimal places; default is 4|

Remarks:

* If no option is specified, the integers are drawn normalvariate around zero.
* If only bounds are specified, the integers are drawn uniform.
* If *normal* is selected as distribution and the mu option is not explicitely set, mu is either set to zero or to the center of the bounds.
* If *pareto* is selected as distribution and there is no lower bound specified, the bound is set to zero.
* If *normal* or *pareto* is selected as distribution, the specified bounds may result in clipping behavior.


### Blindtext

A few sentences of text.

Usage:

```
# Blind = blindtext
#
{Blind}
```


### Concatenation

Usage:

```
# PosInt = int min=0
# Pair = "(" + {int} + "," + PosInt + ")"
#
{Pair}
```

Note that:

* concrete string instances are enquoted,
* variables are used as they are,
* expressions may be embedded in braces,
* nesting is possible.


### Union Types

One of multiple specified types is randomly selected on each evaluation.

Usage:

```
# PosInt = int min=0
# MyUnion = PosInt | {"(" + PosInt + ")"} | ""
#
{MyUnion}
```

or

```
# PosInt = int min=0
# MyUnion = PosInt 77 | {"(" + PosInt + ")"} | "" 2
#
{MyUnion}
```

In the basic form shown in the first example above, all specified types have the same probability to be chosen. By adding positive integers the probabilities can be customized. In the second example, the probabilities are 77/80 to choose `PosInt`, 2/80 to choose `""` and 1/80 to choose `"(" + PosInt + ")"`.

Note that:

* concrete string instances are enquoted,
* variables are used as they are,
* expressions may be embedded in braces,
* nesting is possible.


### Dates

Usage:

```
# MyDate = date ...options...
#
{MyDate}
```

Summary of Options:

|Option|Format|Description|
|---|---|---|
|min|date (iso or german)|lower bound of the domain; inclusive; default is 1990-01-01|
|max|date (iso or german)|upper bound of the domain; inclusive; default is today|

There are no more features supported yet.


### Regexes

Usage:

```
# MyRegex = regex /[a-z]+/
#
{MyRegex}
```

The package [exrex](https://pypi.org/project/exrex/) is used to identify matching strings.


### List Imports

Draws items randomly from a file.

Usage:

```
# Lastname = list lists/lastname_german.list ...options...
#
{Lastname}
```

Summary of Options:

|Option|Format|Description|
|---|---|---|
|sep|string|separator used to split file into items; default is newline|
|dist|uniform, natural, desc, asc, pareto|specifies the probability distribution used to draw from the domain; default is uniform|
|sigma_option|positive float|parameter used to customize the probability distribution if dist=normal|(max-min)/4|
|paretoshape_option|positive float|parameter used to customize the probability distribution if dist=pareto|1.0|


### Schema Imports

Draws items randomly by using another schema.

Usage:

```
# Fullname = import name.schema
#
{Fullname}
```


### Alternation Types

Alternations are like union types but fix the chosen type for the whole data row. Alternations can only be used with variables and are characterized by the `||` operator.

Usage:

```
# MyInt = int min=0
# MyDec = decimal min=0
# MyNum = MyInt || MyAlt
#
({MyNum}, {MyNum})
```


### Values and Functions

Sometimes you want to refer to existing values. For example, you need to sum up some previously used values. Values and functions are made for that.

Basic usage:

```
# concat = function {$0 + $1 + $2}
# concat_string << concat "hello" " " "world"
#
{concat_string}
```

or

```
# Points = int min=0 max=3
# sum = function {sum([int(arg) for arg in $args])}
# p1 << Points
# p2 << Points
# p3 << Points
# psum << sum p1 p2 p3
#
{p1},{p2},{p3},{psum}
```

Note that:

* values can be specified by using `<<` instead of the equality sign,
* function evaluation is straight forward,
* functions can be specified by python expressions,
* those expressions are enriched with the special variables `$args` and `$0`, `$1`, ...; `$args` contains the argument list on evaluation
* all function arguments are strings eventually,
* function evaluation is not allowed to be nested.

There is an advanced feature: function evaluation modifiers. Consider the following example:
```
# Points = {int min=0 max=3} | "" | "--"
# sum = function {sum([int(arg) for arg in $args])}
# p1 << Points
# p2 << Points
# p3 << Points
# psum << sum p1 p2 p3 {"" < "0"} {"--" > ""}
#
{p1},{p2},{p3},{psum}
```

Since `Points` can now take special values, we should take action. In the function evaluation we specified two modifiers behind the arguments. The first one is an input modifier. It specifies that each argument with an empty value takes the value `0` instead. The second modifier is an output modifier. It specifies that the result of the whole evaluation is the empty value if at least one of the arguments has the value `--`.


### Arrays

Not supported yet.




## License Information

Badger - A Schema-Based Data Generator
Copyright (C) 2023 Mirko Schulze

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.