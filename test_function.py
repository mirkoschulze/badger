# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import unittest
from main import generate
from helpers import resolve_docstring


class FunctionTest(unittest.TestCase):

    def test_function_call_including_constants(self):
        code = """
            # concat = function {$0 + $1}
            # i << concat "foo" "bar"
            #
            {i}
        """
        code = resolve_docstring(code)
        sample = generate(
            code,
            1
        )
        item = sample[0]
        self.assertTrue(item == "foobar")


    def test_function_call_including_variables(self):
        code = """
            # concat = function {$0 + $1}
            # String = "foo"
            # s << String
            # i << concat s "bar"
            #
            {i}
        """
        code = resolve_docstring(code)
        sample = generate(
            code,
            1
        )
        item = sample[0]
        self.assertTrue(item == "foobar")


    def test_function_call_with_variable_argument_list(self):
        code = """
            # sum = function {sum([int(i) for i in $args])}
            # i << sum "1" "2" "3"
            #
            {i}
        """
        code = resolve_docstring(code)
        sample = generate(
            code,
            1
        )
        item = sample[0]
        self.assertTrue(item == "6")


    def test_function_call_with_input_modifier(self):
        code = """
            # sum = function {sum([int(i) for i in $args])}
            # i << sum "1" "" "3" {"" < "2"}
            #
            {i}
        """
        code = resolve_docstring(code)
        sample = generate(
            code,
            1
        )
        item = sample[0]
        self.assertTrue(item == "6")


    def test_function_call_with_output_modifier(self):
        code = """
            # sum = function {sum([int(i) for i in $args])}
            # i << sum "1" "" "3" {"" > "6"}
            #
            {i}
        """
        code = resolve_docstring(code)
        sample = generate(
            code,
            1
        )
        item = sample[0]
        self.assertTrue(item == "6")


    def test_function_call_with_conflicting_modifiers(self):
        code = """
            # sum = function {sum([int(i) for i in $args])}
            # i << sum "1" "" "3" {"" > "6"} {"" < "0"}
            # j << sum "1" "" "3" {"" < "0"} {"" > "6"} 
            #
            {i} {j}
        """
        code = resolve_docstring(code)
        sample = generate(
            code,
            1
        )
        nums = sample[0].split(" ")
        self.assertTrue(nums[0] == "6" and nums[1] == "4")


if __name__ == '__main__':
    unittest.main()