# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from lark import Token, Tree


class InputException(Exception):
    pass


def extract_line_from_ast(ast):
    if isinstance(ast, Tree):
        try:
            line = ast.meta.line
        except AttributeError:
            pass
        else:
            return line
    if isinstance(ast, Token):
        try:
            line = ast.line
        except AttributeError:
            pass
        else:
            return line
    return None


def raise_input_exception(msg, ast_or_line=None):
    exception_param = {"msg": msg}
    if ast_or_line is None:
        raise InputException(exception_param)
    if isinstance(ast_or_line, int):
        exception_param["line"] = str(ast_or_line)
    elif isinstance(ast_or_line, Token) or isinstance(ast_or_line, Token):
        line = extract_line_from_ast(ast_or_line)
        if line is not None:
            exception_param["line"] = str(line)
    raise InputException(exception_param)


if __name__ == '__main__':
    pass