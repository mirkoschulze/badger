# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import unittest
from hist import NaturalCenteredHist
from list_sampling import *


class ListSamplingTest(unittest.TestCase):

    def test_determine_num_of_records_in_file(self):
        path = "testres/hundred.file"
        sep = "\n"
        result = determine_num_of_records_in_file(path, sep)
        self.assertTrue(result == 100)


    def test_extract_sample(self):
        path = "testres/hundred.file"
        sep = "\n"
        indizes = [0, 3, 42, 99]
        result = extract_sample(path, sep, indizes)
        self.assertTrue(len(result) == 4)
        self.assertTrue(result.count("0") == 1)
        self.assertTrue(result.count("3") == 1)
        self.assertTrue(result.count("42") == 1)
        self.assertTrue(result.count("99") == 1)


    def test_sample_from_file_uniform(self):
        path = "testres/hundred.file"
        sep = "\n"
        sample = sample_from_file_uniform(path, sep, 10_000)
        hist = NaturalCenteredHist(sample, 0, 99)
        self.assertTrue(hist.freqs_between_are_between(0,99,50,150))
        self.assertTrue(abs(sum(hist.diffs_between(0,99))) < 50)


    def test_sample_from_file_desc(self):
        path = "testres/hundred.file"
        sep = "\n"
        sample = sample_from_file_desc(path, sep, 10_000)
        hist = NaturalCenteredHist(sample, 0, 99)
        self.assertTrue(hist.freqs_between_are_between(0,4,150,250))
        self.assertTrue(hist.freq_at_is_between(50,50,150))
        self.assertTrue(hist.freqs_between_are_between(95,99,0,30))
        self.assertTrue(sum(hist.diffs_between(0,99)) < -133)


    def test_sample_from_file_asc(self):
        path = "testres/hundred.file"
        sep = "\n"
        sample = sample_from_file_asc(path, sep, 10_000)
        hist = NaturalCenteredHist(sample, 0, 99)
        self.assertTrue(hist.freqs_between_are_between(0,4,0,30))
        self.assertTrue(hist.freq_at_is_between(50,50,150))
        self.assertTrue(hist.freqs_between_are_between(95,99,150,250))
        self.assertTrue(sum(hist.diffs_between(0,99)) > 133)


    def test_read_cyclic_from_file(self):
        path = "testres/hundred.file"
        sep = "\n"
        sample = read_cyclic_from_file(path, sep, 342)
        self.assertTrue(sample[0] == "0")
        self.assertTrue(sample[42] == "42")
        self.assertTrue(sample[100] == "0")
        self.assertTrue(sample[266] == "66")
        self.assertTrue(sample[341] == "41")


    def test_sample_from_file_paretovariate_with_resampling(self):
        path = "testres/hundred.file"
        sep = "\n"
        sample = sample_from_file_paretovariate_with_resampling(
            path, sep, 10_000, 1, 10
        )
        hist = NaturalCenteredHist(sample, 0, 99)
        self.assertTrue(hist.freq_at_is_between(0, 800, 1200))
        self.assertTrue(hist.diffs_between_are_between(0, 10, -3000, 30))
        self.assertTrue(hist.freqs_between_are_between(10, 20, 40, 400))
        self.assertTrue(hist.freqs_between_are_between(20, 40, 20, 200))
        self.assertTrue(hist.freqs_between_are_between(40, 99, 0, 100))


if __name__ == '__main__':
    unittest.main()