# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from lark import Token, Tree


def resolve_string(string):
    """
    This function trims outer quotes if present and resolves all kinds of
    backslash based escape sequences. It works as intended only if quotes
    are part of the escape strategy.
    """
    if string == "": return ""
    if string[0] != "\"":
        return resolve_escaped_string(string)
    if string[-1] != "\"":
        e_msg = resolve_docstring(
            """
            This string uses unescaped quote chars
            but not in the way this function intends.
            """,
            ignore_linebreaks=True
        )
        raise ValueError(e_msg)
    return resolve_escaped_string(string[1:-1])


def resolve_escaped_string(string):
    """
    This function resolves all kinds of backslash based escape sequences.
    The sequences for newline and tab are not resolved.
    """
    if string == "": return ""
    output = ""
    i = 0
    while i < len(string):
        if string[i] != "\\":
            output += string[i]
            i += 1
            continue
        if i+1 >= len(string):
            raise ValueError("Escaping is broken.")
        if string[i:i+2] in ["\\n", "\\t"]:
            output += string[i:i+2]
            i += 2
            continue
        output += string[i+1]
        i += 2
    return output


def enquote_and_escape(string, chars_to_escape=["{", "}", "\n", "\t", "\""]):
    string = string.replace("\\", "\\\\")
    for char in chars_to_escape:
        string = string.replace(char, "\\" + char)
    return "\"" + string + "\""


def resolve_boolean(string):
    if string in ["true", "True", "TRUE", "yes", "y", "on"]:
        return True
    if string in ["false", "False", "FALSE", "no", "n", "off"]:
        return False
    raise Exception("BOOLEAN invalid.")


def resolve_docstring(docstring, ignore_linebreaks=False):
    '''
    Takes a docstring and
        * cuts away first line and last line,
        * trims each line.
    '''
    delim = "\n"
    if ignore_linebreaks:
        delim = " "
    return delim.join(
        map(lambda x: x.strip(), docstring.split("\n")[1:-1])
    )


def is_int(s):
    if isinstance(s, str):
        if s.startswith("+") or s.startswith("-"):
            return s[1:].isdigit()
        return s.isdigit()
    if isinstance(s, (list, set, map)):
        return all(map(
            lambda item: is_int(item),
            s
        ))
    if isinstance(s, int):
        return True
    return False


def is_decimal_with_n_places(x, p):
    if isinstance(x, (str, float)):
        d = float(x)
        return round(d, p) == d
    if isinstance(x, (list, set, map)):
        return all(map(
            lambda item: is_decimal_with_n_places(item, p),
            x
        ))
    if isinstance(x, int) and p != 0:
        return False
    return False


def shift_lower_bound_to_conform_to_precision(bound, precision):
    if precision < 0:
        raise ValueError("Precision can only be non-negative.")
    return round(
        round(bound, precision) - 0.4999 / 10 ** precision,
        precision + 1
    )


def shift_upper_bound_to_conform_to_precision(bound, precision):
    if precision < 0:
        raise ValueError("Precision can only be non-negative.")
    return round(
        round(bound, precision) + 0.4999 / 10 ** precision,
        precision + 1
    )


def unite_dicts(a, b):
    return dict(b, **a)


if __name__ == '__main__':
    pass