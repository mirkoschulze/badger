# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from lark import Lark, Token, Tree, tree
import exrex
from helpers import (
    resolve_boolean,
    resolve_docstring,
    resolve_escaped_string,
    resolve_string,
    shift_lower_bound_to_conform_to_precision,
    shift_upper_bound_to_conform_to_precision,
)
from input_exception import raise_input_exception
from datetime import datetime
from datetime_sampling import FORMAT, identify_date_format


PATH_TO_GRAMMAR = 'badger.bnf'
grammar_file = open(PATH_TO_GRAMMAR, 'r')
lark = Lark(grammar_file.read(), parser='earley')
grammar_file.close()


OPTION_KEY = {
    "min_int_option": "min",
    "min_decimal_option": "min",
    "min_date_option": "min",
    "max_int_option": "max",
    "max_decimal_option": "max",
    "max_date_option": "max",
    "precision_option": "precision",
    "padding_option": "padding",
    "format_num_option": "format",
    "format_date_option": "format",
    "dist_num_option": "dist",
    "dist_list_option": "dist",
    "mu_option": "mu",
    "sigma_option": "sigma",
    "paretoshape_option": "paretoshape",
    "paretofactor_option": "paretofactor",
    "sep_option": "sep"
}
KEYWORDS = [
    "blindtext",
    "int",
    "decimal",
    "date",
    "time",
    "datetime",
    "regex",
    "list",
    "schema",
    "list_import",
    "schema_import",
    "import_schema",
    "import_list",
    "import",
    "function",
    "def",
    "eval",
    "type",
    "var",
    "val",
    "in",
    "if",
    "then",
    "else",
    "predicate"
]
VARIABLE = "identifier"
STRING_LITERAL = "string_literal"

BODY_TEXT = "body_text"
SIMPLE_EMBEDDING = "simple_embedding"
ARRAY_EMBEDDING = "array_embedding"

INVALID_SPEC_ERROR_MSG = resolve_docstring(
    """
    Type not correctly specified. Revise the specification.
    """
)

def parse(code):
    try:
        ast = lark.parse(code)
    except Exception as e:
        raise_input_exception(str(e))
    return ast


def analyze(code):
    ast = parse(code)
    body_ast = ast.children[1]
    var_specs, individuals = analyze_head(ast.children[0])
    body = analyze_body(body_ast, var_specs)
    return (body, var_specs, individuals)


def analyze_head(head_ast):
    var_specs = {}
    individuals = []
    for definition_ast in head_ast.children:
        analyze_definition(definition_ast, var_specs, individuals)
    verify_acyclicity(var_specs)
    return (var_specs, individuals)


def analyze_definition(definition_ast, var_specs, individuals):
    identifier = definition_ast.children[0].value
    line = definition_ast.children[0].line
    if identifier in var_specs:
        raise_input_exception(
            "Identifier '" + identifier + "' already defined.",
            definition_ast.children[0]
        )
    if identifier.lower() in KEYWORDS:
        raise_input_exception(
            "'" + identifier 
                + "' is reserved and cannot be used as an identifier.",
            definition_ast.children[0]
        )
    definition_type = definition_ast.data.value
    child_ast = definition_ast.children[1]
    if definition_type == "type_def":
        var_specs[identifier] = analyze_type_expr(child_ast)
    elif definition_type == "func_def":
        var_specs[identifier] = analyze_func_def_expr(child_ast)
    elif definition_type == "array_def":
        var_specs[identifier] = analyze_array_def_expr(child_ast)
    elif definition_type == "array_individual_def":
        var_specs[identifier] = analyze_array_def_expr(child_ast)
        individuals.append(identifier)
    elif definition_type == "alternation_def":
        var_specs[identifier] = analyze_alternation_expr(child_ast)
    elif definition_type == "individual_def":
        var_specs[identifier] = analyze_type_expr(child_ast)
        individuals.append(identifier)
    else:
        raise Exception("Unknown definition type.")


def analyze_type_expr(expr_ast):
    expr_type = expr_ast.data.value
    if expr_type == VARIABLE:
        identifier = expr_ast.children[0].value
        if identifier in ["blindtext", "Blindtext", "BLINDTEXT"]:
            return {"type": "blindtext"}
        if identifier in ["int", "Int", "INT"]:
            sub_spec = analyze_int_type_expr()
            sub_spec["type"] = "int"
            return sub_spec
        if identifier in ["decimal", "Decimal", "DECIMAL"]:
            sub_spec = analyze_decimal_type_expr()
            sub_spec["type"] = "decimal"
            return sub_spec
        if identifier in ["date", "Date", "DATE"]:
            sub_spec = analyze_date_type_expr()
            sub_spec["type"] = "date"
            return sub_spec
        return {"type": VARIABLE, "target": identifier}
    if expr_type == STRING_LITERAL:
        string = resolve_string(
            expr_ast.children[0].value
        )
        return {"type": STRING_LITERAL, "string": string}
    if expr_type == "type_construction_expr":
        return analyze_type_construction_expr(expr_ast.children[0])
    if expr_type == "func_eval_expr":
        return analyze_type_construction_expr(expr_ast)
    raise Exception(
        "Expression analysis is incomplete; cannot handle expression type '"
        + str(expr_type) + "'."
    )


def analyze_type_construction_expr(expr_ast):
    type_string = expr_ast.data.value[:-5]
    spec = {}
    if type_string == "int":
        spec = analyze_int_type_expr(expr_ast)
    elif type_string == "decimal":
        spec = analyze_decimal_type_expr(expr_ast)
    elif type_string == "date":
        spec = analyze_date_type_expr(expr_ast)
    elif type_string == "regex":
        spec = analyze_regex_expr(expr_ast)
    elif type_string == "list_import":
        spec = analyze_list_import_type_expr(expr_ast)
    elif type_string == "schema_import":
        spec = analyze_schema_import_type_expr(expr_ast)
    elif type_string == "concatenation":
        targets = [analyze_type_expr(child) for child in expr_ast.children]
        spec = {"targets": targets}
    elif type_string == "union":
        spec = analyze_union(expr_ast)
    elif type_string == "func_eval":
        return analyze_func_eval(expr_ast)
    spec["type"] = type_string
    return spec


def analyze_int_type_expr(expr_ast=None):
    spec = {}
    if expr_ast:
        spec = extract_options(expr_ast.children)

    if "dist" not in spec:
        if "min" in spec and "max" in spec:
            spec["dist"] = "uniform"
        elif "min" not in spec and "max" not in spec:
            spec["dist"] = "normal"
        else:
            raise_input_exception(INVALID_SPEC_ERROR_MSG, expr_ast)

    if "min" in spec:
        spec["min"] = int(round(spec["min"]))
    if "max" in spec:
        spec["max"] = int(round(spec["max"]))

    dist = spec["dist"]

    if dist == "uniform":
        if "min" not in spec:
            spec["min"] = -2_147_483_648
        if "max" not in spec:
            spec["max"] = +2_147_483_647
    
    if dist in ["desc", "asc"]:
        if "min" not in spec or "max" not in spec:
            raise_input_exception(
                "Min and max have to be specified.",
                expr_ast
            )
    
    if dist == "normal":
        if "sigma" in spec:
            spec["sigma"] = spec["sigma"]
        else:
            spec["sigma"] = 10
        if "mu" in spec:
            spec["mu"] = spec["mu"]
        elif "min" not in spec and "max" not in spec:
            spec["mu"] = 0
        elif "min" in spec and "max" in spec:
            spec["mu"] = spec["min"] + (spec["max"] - spec["min"]) / 2
        if "min" in spec and spec["min"] >= spec["mu"]:
            raise_input_exception("Min must be smaller than mu.", expr_ast)
        if "max" in spec and spec["max"] <= spec["mu"]:
            raise_input_exception("Max must be greater than mu.", expr_ast)

    if dist == "pareto":
        if "max" in spec and "min" not in spec:
            raise_input_exception(
                "Min has to be specified if max is specified.",
                expr_ast
            )
        if "min" not in spec:
            spec["min"] = 0
        spec["min"] = shift_lower_bound_to_conform_to_precision(spec["min"], 0)
        if "paretoshape" in spec:
            spec["paretoshape"] = spec["paretoshape"]
        else:
            spec["paretoshape"] = 1.0
        if "paretofactor" in spec:
            spec["paretofactor"] = spec["paretofactor"]
        else:
            spec["paretofactor"] = 1.0

    if "min" in spec and "max" in spec and spec["min"] > spec["max"]:
        raise_input_exception(
            "Min and max are not correctly specified.",
            expr_ast
        )

    return spec


def analyze_decimal_type_expr(expr_ast=None):
    spec = {}
    if expr_ast:
        spec = extract_options(expr_ast.children)

    if "dist" not in spec:
        if "min" in spec and "max" in spec:
            spec["dist"] = "uniform"
        elif "min" not in spec and "max" not in spec:
            spec["dist"] = "normal"
        else:
            raise_input_exception(INVALID_SPEC_ERROR_MSG, expr_ast)

    if "precision" in spec:
        if spec["precision"] > 8:
            raise_input_exception(
                "Precision cannot be greater than eight.",
                expr_ast
            )
    else:
        spec["precision"] = 4
    if "min" in spec:
        spec["min"] = round(spec["min"], spec["precision"])
    if "max" in spec:
        spec["max"] = round(spec["max"], spec["precision"])

    dist = spec["dist"]

    if dist == "uniform":
        if "min" not in spec:
            spec["min"] = -2_147_483_648
        if "max" not in spec:
            spec["max"] = +2_147_483_647
        spec["min"] = shift_lower_bound_to_conform_to_precision(
            spec["min"],
            spec["precision"]
        )
        spec["max"] = shift_upper_bound_to_conform_to_precision(
            spec["max"],
            spec["precision"]
        )

    if dist in ["desc", "asc"]:
        if "min" not in spec or "max" not in spec:
            raise_input_exception(
                "Min and max have to be specified.",
                expr_ast
            )
        spec["min"] = shift_lower_bound_to_conform_to_precision(
            spec["min"],
            spec["precision"]
        )
        spec["max"] = shift_upper_bound_to_conform_to_precision(
            spec["max"],
            spec["precision"]
        )
    
    if dist == "normal":
        if "sigma" in spec:
            spec["sigma"] = spec["sigma"]
        else:
            spec["sigma"] = 10
        if "mu" in spec:
            spec["mu"] = spec["mu"]
        elif "min" not in spec and "max" not in spec:
            spec["mu"] = 0
        elif "min" in spec and "max" in spec:
            spec["mu"] = spec["min"] + (spec["max"] - spec["min"]) / 2
        if "min" in spec and spec["min"] >= spec["mu"]:
            raise_input_exception("Min must be smaller than mu.", expr_ast)
        if "max" in spec and spec["max"] <= spec["mu"]:
            raise_input_exception("Max must be greater than mu.", expr_ast)

    if dist == "pareto":
        if "max" in spec and "min" not in spec:
            raise_input_exception(
                "Min has to be specified if max is specified.",
                expr_ast
            )
        if "min" not in spec:
            spec["min"] = 0
        spec["min"] = shift_lower_bound_to_conform_to_precision(
            spec["min"],
            spec["precision"]
        )
        if "paretoshape" in spec:
            spec["paretoshape"] = spec["paretoshape"]
        else:
            spec["paretoshape"] = 1.0
        if "paretofactor" in spec:
            spec["paretofactor"] = spec["paretofactor"]
        else:
            spec["paretofactor"] = 1.0

    if "min" in spec and "max" in spec and spec["min"] > spec["max"]:
        raise_input_exception(
            "Min and max are not correctly specified.",
            expr_ast
        )

    return spec


def analyze_date_type_expr(expr_ast=None):
    spec = {}
    if expr_ast:
        spec = extract_options(expr_ast.children)

    date_error_msg = resolve_docstring(
        """
        Date is not properly specified.
        Note that only iso format (1999-12-31)
        or german format (31.12.1999) are supported.
        """,
        ignore_linebreaks = True
    )

    if "min" in spec:
        try:
            date_format = identify_date_format(spec["min"])
            spec["min"] = datetime.strptime(spec["min"], date_format)
        except Exception as e:
            raise_input_exception(date_error_msg, expr_ast)
    else:
        spec["min"] = datetime.fromisoformat('1990-01-01')

    if "max" in spec:
        try:
            date_format = identify_date_format(spec["max"])
            spec["max"] = datetime.strptime(
                spec["max"],
                date_format
            )
        except Exception as e:
            raise_input_exception(date_error_msg, expr_ast)
    else:
        spec["max"] = datetime.today()

    if "format" in spec:
        try:
            output_format = FORMAT[resolve_string(spec["format"])]
            datetime.fromisoformat('1990-01-01').strftime(output_format)
            spec["format"] = output_format 
        except Exception as e:
            raise_input_exception(
                "Date format is not properly specified."
                ,expr_ast
            )
    else:
        spec["format"] = '%Y-%m-%d'

    return spec


def analyze_regex_expr(expr_ast):
    regex = expr_ast.children[0].value[1:-1]
    try:
        exrex.getone(regex)
    except Exception as e:
        raise_input_exception("Invalid regex.", expr_ast)
    return {"regex": regex}


def analyze_list_import_type_expr(expr_ast):
    spec = extract_options(expr_ast.children[1:])
    spec["cache"] = []
    spec["source"] = expr_ast.children[0].value
    if not "dist" in spec:
        spec["dist"] = "uniform"
    if not "sep" in spec:
        spec["sep"] = "\n"
    if "paretoshape" not in spec:
        spec["paretoshape"] = 1.0
    if "paretofactor" not in spec:
        spec["paretofactor"] = 1.0
    return spec


def analyze_schema_import_type_expr(expr_ast):
    source = expr_ast.children[0].value
    code = ""
    try:
        with open(source, 'r') as file:
            code = file.read()
    except Exception as e:
        raise_input_exception("File could not be opened.", expr_ast)
    try:
        body, var_specs, individuals = analyze(code)
    except Exception as e:
        raise_input_exception("Invalid schema.", expr_ast)
    return {
        "body": body,
        "cache": [],
        "individuals": individuals,
        "var_specs": var_specs
    }


def analyze_union(expr_ast):
    freqs = []
    targets = []
    for child in expr_ast.children:
        freq = 1
        if child.data.value[0:8] == "weighted":
            freq = int(child.children[1].value)
            child = child.children[0]
        freqs.append(freq)
        targets.append(analyze_type_expr(child))
    return {
        "freqs": freqs,
        "targets": targets
    }


def analyze_alternation_expr(expr_ast):
    spec = analyze_union(expr_ast)
    spec["type"] = "alternation"
    return spec


def analyze_func_def_expr(func_def_expr_ast):
    code = resolve_escaped_string(func_def_expr_ast.children[0].value)
    return {
        "type": "function",
        "code": code
    }


def analyze_array_def_expr(array_expr_ast):
    if array_expr_ast.data.value == "array_def_expr":
        length = int(array_expr_ast.children[0].value)
        target = analyze_type_expr(array_expr_ast.children[1])
        return {
            "type": "array",
            "subtype": "base",
            "length": length,
            "target": target
        }
    if array_expr_ast.data.value == "sub_array_expr":
        target = array_expr_ast.children[0].value
        from_index = int(array_expr_ast.children[1].value)
        to_index = int(array_expr_ast.children[2].value)
        return {
            "type": "array",
            "subtype": "sub",
            "from": from_index,
            "to": to_index,
            "target": target
        }
    


def analyze_func_eval(func_eval_expr_ast):
    func_term = func_eval_expr_ast.children[0]
    func = func_term.children[0].value
    args = []
    for arg in func_term.children[1:]:
        args.append(analyze_type_expr(arg))
    modifiers = []
    input_modifiers = {}
    output_modifiers = {}
    if len(func_eval_expr_ast.children) > 1:
        modifiers = func_eval_expr_ast.children[1].children
    for modifier in modifiers:
        val0 = resolve_string(modifier.children[0].value)
        if val0 in input_modifiers:
            continue
        if val0 in output_modifiers:
            continue
        val1 = resolve_string(modifier.children[1].value)
        if modifier.data.value == "input_modifier":
            input_modifiers[val0] = val1
        elif modifier.data.value == "output_modifier":
            output_modifiers[val0] = val1
    return {
        "type": "evaluation",
        "function": func,
        "arguments": args,
        "input_modifiers": input_modifiers,
        "output_modifiers": output_modifiers
    }


def extract_options(option_asts):
    options = {}
    for option_ast in option_asts:
        option_key = OPTION_KEY[option_ast.children[0].data.value]
        token = option_ast.children[0].children[0]
        value = token.value
        if token.type == "STRING_LITERAL":
            value = resolve_string(value)
        elif token.type in [
            "INT", "POSINT", "UINT"
        ]:
            value = int(value)
        elif token.type in [
            "DECIMAL", "POSDECIMAL", "UDECIMAL", "FLOAT", "POSFLOAT", "UFLOAT"
        ]:
            value = float(value)
        elif token.type in ["DIST_NUM_VALUE", "DIST_LIST_VALUE"]:
            value = value.lower()
        elif token.type == "BOOLEAN":
            value = resolve_boolean(value)
        elif token.type == "BODY_TEXT":
            value = resolve_escaped_string(value)
        options[option_key] = value
    return options


def analyze_body(body_ast, var_specs):
    body = []
    for node in body_ast.children:
        if node.data.value == BODY_TEXT:
            body.append((
                BODY_TEXT, 
                resolve_escaped_string(node.children[0].value)
            ))
            continue
        if node.data.value == SIMPLE_EMBEDDING:
            token = node.children[0]
            if token.value in ["blindtext", "Blindtext", "BLINDTEXT"]:
                var_specs[token.value] = {"type": "blindtext"}
                body.append((SIMPLE_EMBEDDING, "blindtext"))
            elif token.value in ["int", "Int", "INT"]:
                var_specs[token.value] = analyze_int_type_expr()
                var_specs[token.value]["type"] = "int"
                body.append((SIMPLE_EMBEDDING, "int"))
            elif token.value in ["decimal", "Decimal", "DECIMAL"]:
                var_specs[token.value] = analyze_decimal_type_expr()
                var_specs[token.value]["type"] = "decimal"
                body.append((SIMPLE_EMBEDDING, "decimal"))
            elif token.value in ["date", "Date", "DATE"]:
                var_specs[token.value] = analyze_date_type_expr()
                var_specs[token.value]["type"] = "date"
                body.append((SIMPLE_EMBEDDING, "date"))
            elif token.value not in var_specs:
                raise_input_exception(
                    "Variable '" + token.value +"' is not defined.",
                    token
                )
            elif var_specs[token.value]["type"] == "function":
                msg = (
                    "The identifier '"
                    + token.value 
                    + "' refers to a function and cannot be used this way."
                )
                raise_input_exception(msg, token)
            else:
                body.append((SIMPLE_EMBEDDING, token.value))
            continue
        if node.data.value == ARRAY_EMBEDDING:
            delim = resolve_string(node.children[0].value)
            identifiers = [token.value for token in node.children[1:]]
            for identifier in identifiers:
                if identifier not in var_specs:
                    raise_input_exception(
                        "Variable '" + identifier +"' is not defined.",
                        node
                    )
                elif var_specs[identifier]["type"] == "function":
                    msg = (
                        "The identifier '"
                        + identifier
                        + "' refers to a function and cannot be used this way."
                    )
                    raise_input_exception(msg, node)
            spec = {"identifiers": identifiers, "delim": delim}
            body.append((ARRAY_EMBEDDING, spec))
    return body


def verify_acyclicity(var_specs):
    for var in var_specs.keys():
        spec = var_specs[var]
        if spec["type"] == "function":
            continue
        dfs_on_var_specs(var_specs, var)


def dfs_on_var_specs(var_specs, identifier, marks_=None):
    marks = {}
    if marks_ is None:
        marks = {
            key: "unknown"
            for key in var_specs.keys()
        }
    else:
        marks = marks_
    marks[identifier] = "under_exploration"
    for next_identifier in referenced_variables(var_specs, identifier):
        if next_identifier not in var_specs:
            raise_input_exception(
                "Variable '" + next_identifier + "' is not defined."
            )
        if var_specs[next_identifier]["type"] == "function":
            raise_input_exception(
                "The definition of '"
                + identifier
                + "' uses a function without evaluation."
            )
        if (
            var_specs[identifier]["type"] == "array" and 
            var_specs[identifier]["subtype"] == "sub" and
            var_specs[next_identifier]["type"] != "array"
        ):
            raise_input_exception(
                "The definition of '"
                + identifier
                + "' cannot use anything else than a list."
            )
        if (
            (var_specs[identifier]["type"] != "array" or
            var_specs[identifier]["subtype"] != "sub") and
            var_specs[next_identifier]["type"] == "array"
        ):
            raise_input_exception(
                "The definition of '"
                + identifier
                + "' uses a list illegaly."
            )
        if marks[next_identifier] == "under_exploration":
            raise_input_exception(
                "Variable '" + next_identifier + "' leads to a cycle."
            )
        if marks[next_identifier] == "unknown":
            dfs_on_var_specs(var_specs, next_identifier, marks)
    marks[identifier] == "fully_explored"


def referenced_variables(var_specs, identifier):
    spec = var_specs[identifier]
    if spec["type"] == VARIABLE:
        return [spec["target"]]
    if spec["type"] in ["concatenation", "union", "alternation"]:
        return [
            target["target"]
            for target in spec["targets"]
            if target["type"] == VARIABLE
        ]
    if spec["type"] == "array" and spec["subtype"] == "sub":
        return [spec["target"]]
    return []
