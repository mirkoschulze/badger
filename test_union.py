# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import unittest
from helpers import resolve_docstring, is_int
from main import generate


class UnionTest(unittest.TestCase):

    def test_enum(self):
        sample = generate(
            "# i = \"foo\" | \"bar\"\n{i}",
            1
        )
        s = sample[0]
        self.assertTrue(s == "foo" or s == "bar")


    def test_enum_with_empty_string(self):
        sample = generate(
            "# i = \"foo\" | \"\"\n{i}",
            10
        )
        self.assertTrue(all(map(
            lambda item: item == "foo" or item == "",
            sample
        )))


    def test_union(self):
        code = """
            # MyInt = int min=0 max=9
            # u = MyInt | \"foo\"
            #
            {u}
        """
        sample = generate(resolve_docstring(code), 10)
        self.assertTrue(all(map(
            lambda item: item == "foo" or item.isdigit(),
            sample
        )))


    def test_numbered_union(self):
        code = """
            # MyInt = int min=0 max=9
            # u = MyInt 3 | \"foo\"
            #
            {u}
        """
        sample = generate(resolve_docstring(code), 1)
        self.assertTrue(all(map(
            lambda item: item == "foo" or item.isdigit(),
            sample
        )))


    def test_union_nested_with_base_spec(self):
        sample = generate(
            "# u = {int} | \"foo\" \n{u}",
            10
        )
        self.assertTrue(all(map(
            lambda item: item == "foo" or is_int(item),
            sample
        )))


if __name__ == '__main__':
    unittest.main()