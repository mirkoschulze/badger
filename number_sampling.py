# Badger - A Schema-Based Data Generator
# Copyright (C) 2023 Mirko Schulze
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from math import ceil, sqrt
from random import uniform, randint, normalvariate, paretovariate


def draw_int_with_desc_probability(minimum, maximum):
    span = maximum - minimum + 1
    return maximum - random_pyramid_layer(span) + 1


def draw_int_with_asc_probability(minimum, maximum):
    span = maximum - minimum + 1
    return minimum + random_pyramid_layer(span) - 1


def random_pyramid_layer(n):
    roll = randint(1, n * (n + 1) // 2)
    return ceil(sqrt(1/4 + 2*roll) - 1/2)


###


def draw_decimal_with_desc_probability(minimum, maximum):
    diff = maximum - minimum
    roll_bound = diff * diff / 2
    roll = uniform(0, roll_bound)
    return maximum - sqrt(2*roll)


def draw_decimal_with_asc_probability(minimum, maximum):
    diff = maximum - minimum
    roll_bound = diff * diff / 2
    roll = uniform(0, roll_bound)
    return minimum + sqrt(2*roll)


def draw_paretovariate(minimum, alpha, factor):
    draw_from_zero = paretovariate(alpha) * factor - factor
    return minimum + draw_from_zero


###


def draw_by_proportion(a):
    cumulated = []
    cumulated.append(a[0])
    for i in range(1, len(a)):
        cumulated.append(cumulated[i-1] + a[i])
    draw = uniform(0, cumulated[-1])
    for i in range(len(cumulated)):
        if draw < cumulated[i]:
            return i
    return len(cumulated)

    

if __name__ == "__main__":
    pass